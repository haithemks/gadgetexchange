﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Http.Routing;
using GadgetExchange.API.Async;
using GadgetExchange.Models.Extension;
using GadgetExchange.Tests.Util.Repository.API;
using GadgetExchange.Tests.Util.Repository.API.Async;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GadgetExchange.Tests.UnitTest.API.Async
{

    [TestClass]
    public class ApiFavoriteGadgetAsyncUnitTest
    {
        private FavoriteGadgetAsyncController _favoritegadgetController;
        public MemoryApiFavoriteGadgetRepositoryAsync Repo;

        [TestInitialize]
        public void Initialize()
        {
            // Arrange
             Repo = new MemoryApiFavoriteGadgetRepositoryAsync();

            _favoritegadgetController = new FavoriteGadgetAsyncController(Repo);
        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void GetAllFavoriteGadgets_Should_Return_AllFavoriteGadgets()
        {
            // Act
            var response = _favoritegadgetController.GetFavoriteGadgets();

            // Assert
            Assert.AreEqual(response.Result.Count(), 3);
        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void GetFavoriteGadgetById_Should_Return_CorrectFavoriteGadget_When_FavoriteGadgetExist()
        {
            // Act
            var actionResult = _favoritegadgetController.GetFavoriteGadget(3).Result;
            var contentResult = actionResult as OkNegotiatedContentResult<FavoriteGadget>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(3, contentResult.Content.GadgetId);

            // Act
            IHttpActionResult actionResultNotFound = _favoritegadgetController.GetFavoriteGadget(15).Result;

            // Assert
            Assert.IsInstanceOfType(actionResultNotFound, typeof(NotFoundResult));
        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void PostFavoriteGadget_Should_AddFavoriteGadget_ToListofFavoriteGadgets()
        {
            var selectedUser = new MemoryApiFavoriteGadgetRepository().MyUsers.Find(x => x.Id == "1");

            // SetUp
            var newFavoriteGadget = new FavoriteGadget
            {
                Id = 4,
                GadgetId = 4,
                User = selectedUser
            };

            var requestUrl = new Uri("https://localhost:44382/api/favoritegadget");
            _favoritegadgetController.Request = new HttpRequestMessage
            {
                RequestUri = requestUrl
            };
            _favoritegadgetController.Configuration = new HttpConfiguration();
            _favoritegadgetController.Configuration.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            _favoritegadgetController.RequestContext.RouteData = new HttpRouteData(
                route: new HttpRoute(),
                values: new HttpRouteValueDictionary { { "controller", "favoritegadget" } });

            // Act
            var actionResult = _favoritegadgetController.PostFavoriteGadget(newFavoriteGadget.Id, selectedUser.Id).Result;
            var createdResult = actionResult as CreatedNegotiatedContentResult<int>;

            // Assert  
            Assert.IsNotNull(createdResult);
            Assert.AreEqual(4, createdResult.Content);
            Assert.AreEqual(requestUrl + "/4", createdResult.Location.AbsoluteUri);

        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void DeleteFavoriteGadget_Should_DeleteFavoriteGadget_If_FavoriteGadgetExist()
        {
            var actionResult = _favoritegadgetController.DeleteFavoriteGadget(3, "1").Result;
            var contentResult = actionResult as OkNegotiatedContentResult<int>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(3, contentResult.Content);

            // Act
            IHttpActionResult actionResultNotFound = _favoritegadgetController.DeleteFavoriteGadget(15, "1").Result;

            // Assert
            Assert.IsInstanceOfType(actionResultNotFound, typeof(NotFoundResult));

        }

        [TestCategory("CRUD")]
        [TestCleanup]
        public void CleanUp()
        {
            _favoritegadgetController.Dispose();
        }

    }
}
