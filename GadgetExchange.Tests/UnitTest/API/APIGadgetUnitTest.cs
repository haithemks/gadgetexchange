﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Http.Routing;
using GadgetExchange.API;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API;
using GadgetExchange.Tests.Util.Repository.API;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GadgetExchange.Tests.UnitTest.API
{

    [TestClass]
    public class ApiGadgetUnitTest
    {
        private GadgetController _gadgetController;

        [TestInitialize]
        public void Initialize()
        {
            // Arrange
            IApiGadgetRepository repo = new MemoryApiGadgetRepsitory();

            _gadgetController = new GadgetController(repo);

        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void GetAllGadgets_Should_Return_AllGadgets()
        {
            // Act
            var response = _gadgetController.GetGadgets();

            // Assert
            Assert.AreEqual(response.Count(), 8);
        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void GetGadgetById_Should_Return_CorrectGadget_When_GadgetExist()
        {
            // Act
            var actionResult = _gadgetController.GetGadget(2);
            var contentResult = actionResult as OkNegotiatedContentResult<Gadget>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(2, contentResult.Content.Id);

            // Act
            IHttpActionResult actionResultNotFound = _gadgetController.GetGadget(20);

            // Assert
            Assert.IsInstanceOfType(actionResultNotFound, typeof(NotFoundResult));
        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void PutGadget_Should_UpdateGadget_If_GadgetExist()
        {
            // Act
            var newGadget = new Gadget
            {
                Id = 2,
                Name = "IPhone7",
                ImagePath = "Path7",
                Description = "new Description",
                ReviewsCount = 35,
                StatusId = Status.Available,
                Cost = (float)10.00
            };

            var actionResult = _gadgetController.PutGadget(2, newGadget);
            var contentResult = actionResult as NegotiatedContentResult<Gadget>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(HttpStatusCode.Accepted, contentResult.StatusCode);
            Assert.AreEqual(contentResult.Content.Description, "new Description");
            Assert.IsNotNull(contentResult.Content);
        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void PostGadget_Should_AddGadget_ToListofGadgets()
        {

            // SetUp
            var newGadget = new Gadget
            {
                Id = 10,
                Name = "IPhone10",
                ImagePath = "Path10",
                Description = "Description 10",
                ReviewsCount = 10,
                StatusId = Status.Available,
                Cost = (float)10.00
            };

            var requestUrl = new Uri("https://localhost:44382/api/gadget");
            _gadgetController.Request = new HttpRequestMessage
            {
                RequestUri = requestUrl
            };
            _gadgetController.Configuration = new HttpConfiguration();
            _gadgetController.Configuration.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            _gadgetController.RequestContext.RouteData = new HttpRouteData(
                route: new HttpRoute(),
                values: new HttpRouteValueDictionary { { "controller", "gadget" } });

            // Act
            var actionResult = _gadgetController.PostGadget(newGadget);
            var createdResult = actionResult as CreatedNegotiatedContentResult<Gadget>;

            // Assert  
            Assert.IsNotNull(createdResult);
            Assert.AreEqual(10, createdResult.Content.Id);
            Assert.AreEqual(requestUrl + "/10", createdResult.Location.AbsoluteUri);

        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void DeleteGadget_Should_DeleteGadget_If_GadgetExist()
        {
            var actionResult = _gadgetController.DeleteGadget(2);
            var contentResult = actionResult as OkNegotiatedContentResult<Gadget>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(2, contentResult.Content.Id);

            // Act
            IHttpActionResult actionResultNotFound = _gadgetController.DeleteGadget(20);

            // Assert
            Assert.IsInstanceOfType(actionResultNotFound, typeof(NotFoundResult));
        }

        [TestCategory("CRUD")]
        [TestCleanup]
        public void CleanUp()
        {
            _gadgetController.Dispose();
        }

    }
}
