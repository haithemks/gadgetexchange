﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Http.Routing;
using GadgetExchange.API;
using GadgetExchange.Models.Extension;
using GadgetExchange.Tests.Util.Repository.API;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GadgetExchange.Tests.IntegrationTest.API
{
    [TestClass]
    public class ApiFavoriteGadgetIntergration
    {
        private FavoriteGadgetController _favoritegadgetController;
        public MemoryApiFavoriteGadgetRepository Repo;

        [TestInitialize]
        public void Initialize()
        {
            Effort.Provider.EffortProviderConfiguration.RegisterProvider();

            // Arrange
            Repo = new MemoryApiFavoriteGadgetRepository();
            _favoritegadgetController = new FavoriteGadgetController(Repo);
        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void GetAllFavoriteGadgets_Should_Return_AllFavoriteGadgets()
        {
            // Act
            var response = _favoritegadgetController.GetFavoriteGadgets();

            // Assert
            Assert.AreEqual(response.Count(), Repo.GetAllFavoriteGadgets().Count());
        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void GetFavoriteGadgetById_Should_Return_CorrectFavoriteGadget_When_FavoriteGadgetExist()
        {
            // Act
            var actionResult = _favoritegadgetController.GetFavoriteGadget(3);
            var contentResult = actionResult as OkNegotiatedContentResult<FavoriteGadget>;
            var selectedGadget = Repo.GetFavoriteGadgetById(3);

            // Assert
            Debug.Assert(contentResult != null, nameof(contentResult) + " != null");
            Assert.AreEqual(selectedGadget.Id, contentResult.Content.Id);

            // Act
            IHttpActionResult actionResultNotFound = _favoritegadgetController.GetFavoriteGadget(15);
            
            // Assert
            Assert.IsInstanceOfType(actionResultNotFound, typeof(NotFoundResult));
        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void PostFavoriteGadget_Should_AddFavoriteGadget_ToListofFavoriteGadgets()
        {
            var selectedUser = new MemoryApiFavoriteGadgetRepository().MyUsers.Find(x => x.Id == "1");

            // SetUp
            var newFavoriteGadget = new FavoriteGadget
            {
                
                GadgetId = 4,
                User = selectedUser
            };

            var requestUrl = new Uri("https://localhost:44382/api/favoritegadget");
            _favoritegadgetController.Request = new HttpRequestMessage
            {
                RequestUri = requestUrl
            };
            _favoritegadgetController.Configuration = new HttpConfiguration();
            _favoritegadgetController.Configuration.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            _favoritegadgetController.RequestContext.RouteData = new HttpRouteData(
                route: new HttpRoute(),
                values: new HttpRouteValueDictionary { { "controller", "favoritegadget" } });

            // Act
            var actionResult = _favoritegadgetController.PostFavoriteGadget(newFavoriteGadget.GadgetId, selectedUser.Id);
            var createdResult = actionResult as CreatedNegotiatedContentResult<int>;
            var selectedGadget = Repo.GetFavoriteGadgetById(newFavoriteGadget.Id);
            // Assert  
            Debug.Assert(createdResult != null, nameof(createdResult) + " != null");
            Assert.AreEqual(selectedGadget.GadgetId, createdResult.Content);

        }

        [TestCategory("CRUD")]
        [TestMethod]
        public void DeleteFavoriteGadget_Should_DeleteFavoriteGadget_If_FavoriteGadgetExist()
        {
            var selectedUser = new MemoryApiFavoriteGadgetRepository().MyUsers.Find(x => x.Id == "1");

            var actionResult = _favoritegadgetController.DeleteFavoriteGadget(3, selectedUser.Id);
            var contentResult = actionResult as OkNegotiatedContentResult<int>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(3, contentResult.Content);

            // Assert
            var deletedItem = Repo.GetFavoriteGadgetById(3);
            Assert.IsNull(deletedItem);

        }

        [TestCategory("CRUD")]
        [TestCleanup]
        public void CleanUp()
        {
            _favoritegadgetController.Dispose();
        }

    }

}
