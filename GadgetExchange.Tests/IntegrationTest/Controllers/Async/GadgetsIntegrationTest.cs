﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using GadgetExchange.Controllers;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.Controllers.Async;
using GadgetExchange.Tests.Util.Repository.Controller.Async;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GadgetExchange.Tests.IntegrationTest.Controllers.Async
{
    [TestClass]
    public class GadgetsIntegrationTest
    {
        private GadgetsController _gadgetsController;

        [TestInitialize]
        public void Initialize()
        {
            Effort.Provider.EffortProviderConfiguration.RegisterProvider();

            // Arrange
            IGadgetRepositoryAsync repo = new MemoryGadgetRepsitoryAsync();

            _gadgetsController = new GadgetsController(repo);

        }

        [TestCategory("Index")]

        [TestMethod]
        public void Index_Where_Page_is_Null_And_Filter_isNull_Should_Return_Null()
        {

            // Act
            var result = _gadgetsController.Index(null);

            var viewresult = result.Result;

            // Assert
            Assert.IsNotNull(viewresult);
        }

        [TestMethod]
        public void Index_Where_Page_is_One_And_Filter_isNull_Should_Return_Nine_Gadget()
        {
            // Act
            var result = _gadgetsController.Index(1);

            var viewresult = result.Result as ViewResult;

            Debug.Assert(viewresult != null, nameof(viewresult) + " != null");

            IEnumerable<Gadget> model = viewresult.Model as IEnumerable<Gadget>;

            // Assert
            if (model != null) Assert.AreEqual(8, model.Count());
        }

        [TestMethod]
        public async Task Index_Where_Page_is_One_And_Filter_is_Mine_Should_Return_My_Gadget()
        {

            // Act
            var result = _gadgetsController.Index(1, Status.Mine.ToString());

            Assert.IsNotNull(result);

            var viewresult = await result as ViewResult;

            Debug.Assert(viewresult != null, nameof(viewresult) + " != null");

            IEnumerable<Gadget> model = viewresult.Model as IEnumerable<Gadget>;

            if (model != null)
            {
                var notMyGadgets = model.Count(x => x.StatusId != Status.Mine);

                // Assert
                Assert.AreEqual(0, notMyGadgets);
            }
        }

        [TestMethod]
        public void ChildAction_StatusSelection_Should_Return_AllStatus()
        {
            // Act
            ViewResult result = _gadgetsController.StatusSelection() as ViewResult;

            if (result != null)
            {
                IEnumerable<Statu> model = result.Model as IEnumerable<Statu>;

                // Assert
                if (model != null) Assert.AreEqual(5, model.Count());
            }
        }


        [TestCleanup]
        public void CleanUp()
        {
            _gadgetsController.Dispose();
        }
    }
}
