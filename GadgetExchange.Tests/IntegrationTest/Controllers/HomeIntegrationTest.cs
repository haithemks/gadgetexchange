﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GadgetExchange.Controllers;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Base;
using GadgetExchange.Respository.Base.Controllers;
using GadgetExchange.Respository.Interface;
using GadgetExchange.Respository.Interface.Controllers;

namespace GadgetExchange.Tests.IntegrationTest.Controllers
{
    [TestClass]
    public class HomeIntegrationTest
    {
        private HomeController _homeController;

        [TestInitialize]
        public void Initialize()
        {
            Effort.Provider.EffortProviderConfiguration.RegisterProvider();
            // Arrange
            IGadgetRepository repo = new GadgetRepository();

            _homeController = new HomeController(repo);

        }

        [TestCategory("Index")]

        [TestMethod]
        public void Index_Where_Page_is_Null_And_Filter_isNull_Should_Return_Null()
        {

           // Act
           ViewResult result = _homeController.Index(null) as ViewResult;

          //  Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Index_Where_Page_is_One_And_Filter_isNull_Should_Return_Nine_Gadget()
        {
            // Act
            ViewResult result = _homeController.Index(1) as ViewResult;

            Debug.Assert(result != null, nameof(result) + " != null");

            IEnumerable<Gadget> model = result.Model as IEnumerable<Gadget>;

            // Assert
            if (model != null) Assert.AreEqual(9, model.Count());
        }

        [TestMethod]
        public void Index_Where_Page_is_One_And_Filter_is_Mine_Should_Return_My_Gadget()
        {

            // Act
            ViewResult result = _homeController.Index(1, Status.Mine.ToString()) as ViewResult;

            Debug.Assert(result != null, nameof(result) + " != null");

            IEnumerable<Gadget> model = result.Model as IEnumerable<Gadget>;

            if (model != null)
            {
                var notMyGadgets = model.Count(x => x.StatusId != Status.Mine);

                // Assert
                Assert.AreEqual(0, notMyGadgets);
            }
        }

        [TestMethod]
        public void ChildAction_StatusSelection_Should_Return_AllStatus()
        {
            // Act
            ViewResult result = _homeController.StatusSelection() as ViewResult;

            if (result != null)
            {
                IEnumerable<Statu> model = result.Model as IEnumerable<Statu>;

                // Assert
                if (model != null) Assert.AreEqual(5, model.Count());
            }
        }


        [TestCleanup]
        public void CleanUp()
        {
            _homeController.Dispose();
        }
    }
}
