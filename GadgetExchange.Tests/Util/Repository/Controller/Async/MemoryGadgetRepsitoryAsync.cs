﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.Controllers.Async;

namespace GadgetExchange.Tests.Util.Repository.Controller.Async
{
    public class MemoryGadgetRepsitoryAsync : IGadgetRepositoryAsync
    {
        private readonly List<Gadget> _myGadgets = new List<Gadget>();

        public MemoryGadgetRepsitoryAsync()
        {
            _myGadgets.AddRange(new List<Gadget>
            {
                new Gadget
                {
                    Id = 1,
                    Name = "IPhone6",
                    ImagePath = "Path6",
                    Description = "IPhone6 Description",
                    ReviewsCount = 3,
                    StatusId = Status.Available,
                    Cost = (float)25.50
                },
                new Gadget
                {
                    Id = 2,
                    Name = "IPhone7",
                    ImagePath = "Path7",
                    Description = "IPhone7 Description",
                    ReviewsCount = 35,
                    StatusId = Status.Available,
                    Cost = (float)10.00
                },
                new Gadget
                {
                    Id = 3,
                    Name = "IPhone8",
                    ImagePath = "Path8",
                    Description = "IPhone8 Description",
                    ReviewsCount = 7,
                    StatusId = Status.Available,
                    Cost = (float)19.99
                },
                new Gadget
                {
                    Id = 4,
                    Name = "Android5",
                    ImagePath = "Path5",
                    Description = "Android5 Description",
                    ReviewsCount = 10,
                    StatusId = Status.Queued,
                    Cost = (float)15.99
                },
                new Gadget
                {
                    Id = 5,
                    Name = "Android4",
                    ImagePath = "Path4",
                    Description = "Android4 Description",
                    ReviewsCount = 13,
                    StatusId = Status.Queued,
                    Cost = (float)25.50
                },
                new Gadget
                {
                    Id = 6,
                    Name = "Tablet1",
                    ImagePath = "Tablet1",
                    Description = "Tablet1 Description",
                    ReviewsCount = 53,
                    StatusId = Status.Mine,
                    Cost = (float)5.99
                },
                new Gadget
                {
                    Id = 7,
                    Name = "Tablet2",
                    ImagePath = "Tablet2",
                    Description = "Tablet2 Description",
                    ReviewsCount = 20,
                    StatusId = Status.Mine,
                    Cost = (float)5.99
                },
                new Gadget
                {
                    Id = 8,
                    Name = "Watch15",
                    ImagePath = "Watch15",
                    Description = "Watch15 Description",
                    ReviewsCount = 10,
                    StatusId = Status.Favorite,
                    Cost = (float)12.99
                }
            });
        }

        public void Dispose()
        {
            _myGadgets.Clear();
        }

        public async Task<IEnumerable<Gadget>> GetGadgets()
        {
            return await Task.Run(() => _myGadgets.AsEnumerable());
        }

        public async Task<IEnumerable<Gadget>> FilterGadgetByStatus(Status status)
        {
            return await Task.Run(() => _myGadgets.Where(x=>x.StatusId == status).AsEnumerable());
        }

        public async Task<Gadget> GetGadgetById(int id)
        {
            return await Task.Run(() => _myGadgets.FirstOrDefault(x => x.Id == id));
        }

        public async Task SetRentalPrice(int id, float price)
        {
            var gadget = await GetGadgetById(id);
            gadget.Cost = price;
        }

        public async Task Save()
        {
            await Task.CompletedTask;
        }
    }
}
