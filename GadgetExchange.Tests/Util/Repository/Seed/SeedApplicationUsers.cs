﻿using System.Data.Entity.Migrations;
using GadgetExchange.Models;
using Microsoft.AspNet.Identity;

namespace GadgetExchange.Tests.Util.Repository.Seed
{
    class SeedApplicationUsers
    {
        public SeedApplicationUsers()
        {

        }

        public SeedApplicationUsers(ApplicationDbContext context)
        {
            Users(context);
        }


        private void Users(ApplicationDbContext context)
        {
            context.Users.AddOrUpdate(

                new ApplicationUser
                {
                    Id = "1",
                    UserName = "Mike",
                    Email = "mike@site.com",
                    EmailConfirmed = true,
                    PasswordHash = new PasswordHasher().HashPassword("Mike123$isGood")
                },
                new ApplicationUser
                {
                    Id = "2",
                    UserName = "Lobna",
                    Email = "lobna@site.com",
                    EmailConfirmed = true,
                    PasswordHash = new PasswordHasher().HashPassword("Lobna123$isGood")
                },
                new ApplicationUser
                {
                    Id = "3",
                    UserName = "Sara",
                    Email = "sara@site.com",
                    EmailConfirmed = true,
                    PasswordHash = new PasswordHasher().HashPassword("Sara123$isGood")
                });
            context.SaveChanges();
        }

        //public async Task<ActionResult> Register(RegisterViewModel model)
        //{

        //        var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
        //        var result = await Microsoft.AspNet.Identity.UserManager.CreateAsync(user, model.Password);
        //        if (result.Succeeded)
        //        {
        //            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

        //            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
        //            // Send an email with this link
        //            // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
        //            // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
        //            // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

        //            //return RedirectToAction("Index", "Home");
        //        }
        //    return null;
        //}

        // If we got this far, something failed, redisplay form
    }


    //public class YourDbContextSeedData
    //{
    //    private ApplicationDbContext _context;

    //    public YourDbContextSeedData(ApplicationDbContext context)
    //    {
    //        _context = context;
    //    }

    //    public async void SeedAdminUser()
    //    {
    //        var user = new ApplicationUser
    //        {
    //            UserName = "Email@email.com",
    //            Email = "Email@email.com",
    //            EmailConfirmed = true,
    //            LockoutEnabled = false,
    //            SecurityStamp = Guid.NewGuid().ToString()
    //        };

    //        var roleStore = new RoleStore<IdentityRole>(_context);

    //        if (!_context.Roles.Any(r => r.Name == "admin"))
    //        {
    //            await roleStore.CreateAsync(new IdentityRole { Name = "admin" });
    //        }

    //        if (!_context.Users.Any(u => u.UserName == user.UserName))
    //        {
    //            var password = new PasswordHasher<ApplicationUser>();
    //            var hashed = password.HashPassword(user, "password");
    //            user.PasswordHash = hashed;
    //            var userStore = new UserStore<ApplicationUser>(_context);
    //            await userStore.CreateAsync(user);
    //            await userStore.AddToRoleAsync(user, "admin");
    //        }

    //        await _context.SaveChangesAsync();
    //    }
    //}
}