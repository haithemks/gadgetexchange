﻿using System.Collections.Generic;
using System.Linq;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;

namespace GadgetExchange.Tests.Util.Repository.Seed
{
    public class SeedHelper
    {
        private SeedApplicationUsers _seedApplicationHelper;
        public SeedHelper()
        {

        }

        public SeedHelper(ApplicationDbContext context)
        {
            SeedGadgets(context);
            SeedUsers(context);
            SeedFavoriteGadgets(context);
        }

        private void SeedGadgets(ApplicationDbContext context)
        {
            context.Gadgets.AddRange(new List<Gadget>
            {
                new Gadget
                {
                    Id = 1,
                    Name = "IPhone6",
                    ImagePath = "Path6",
                    Description = "IPhone6 Description",
                    ReviewsCount = 3,
                    StatusId = Status.Available,
                    Cost = (float) 25.50
                },
                new Gadget
                {
                    Id = 2,
                    Name = "IPhone7",
                    ImagePath = "Path7",
                    Description = "IPhone7 Description",
                    ReviewsCount = 35,
                    StatusId = Status.Available,
                    Cost = (float) 10.00
                },
                new Gadget
                {
                    Id = 3,
                    Name = "IPhone8",
                    ImagePath = "Path8",
                    Description = "IPhone8 Description",
                    ReviewsCount = 7,
                    StatusId = Status.Available,
                    Cost = (float) 19.99
                },
                new Gadget
                {
                    Id = 4,
                    Name = "Android5",
                    ImagePath = "Path5",
                    Description = "Android5 Description",
                    ReviewsCount = 10,
                    StatusId = Status.Queued,
                    Cost = (float) 15.99
                },
                new Gadget
                {
                    Id = 5,
                    Name = "Android4",
                    ImagePath = "Path4",
                    Description = "Android4 Description",
                    ReviewsCount = 13,
                    StatusId = Status.Queued,
                    Cost = (float) 25.50
                },
                new Gadget
                {
                    Id = 6,
                    Name = "Tablet1",
                    ImagePath = "Tablet1",
                    Description = "Tablet1 Description",
                    ReviewsCount = 53,
                    StatusId = Status.Mine,
                    Cost = (float) 5.99
                },
                new Gadget
                {
                    Id = 7,
                    Name = "Tablet2",
                    ImagePath = "Tablet2",
                    Description = "Tablet2 Description",
                    ReviewsCount = 20,
                    StatusId = Status.Mine,
                    Cost = (float) 5.99
                },
                new Gadget
                {
                    Id = 8,
                    Name = "Watch15",
                    ImagePath = "Watch15",
                    Description = "Watch15 Description",
                    ReviewsCount = 10,
                    StatusId = Status.Favorite,
                    Cost = (float) 12.99
                }
            });
            context.SaveChanges();
        }

        private void SeedUsers(ApplicationDbContext context)
        {
            _seedApplicationHelper = new SeedApplicationUsers(context);

        }

        private void SeedFavoriteGadgets(ApplicationDbContext context)
        {
            var mike = context.Users.FirstOrDefault(x => x.UserName == "Mike");
            var lobna = context.Users.FirstOrDefault(x => x.UserName == "Lobna");
            var sara = context.Users.FirstOrDefault(x => x.UserName == "Sara");

            context.FavoriteGadgets.AddRange(new List<FavoriteGadget>
            {
                new FavoriteGadget
                {
                    Id = 1,
                    GadgetId = 1,
                    User = mike
                },
                new FavoriteGadget
                {
                    Id = 2,
                    GadgetId = 3,
                    User = sara
                },
                new FavoriteGadget
                {
                    Id = 3,
                    GadgetId = 2,
                    User = lobna
                },
                new FavoriteGadget
                {
                    Id = 4,
                    GadgetId = 5,
                    User = sara
                },
                new FavoriteGadget
                {
                    Id = 5,
                    GadgetId = 1,
                    User = lobna
                },
                new FavoriteGadget
                {
                    Id = 6,
                    GadgetId = 2,
                    User = sara
                },
                new FavoriteGadget
                {
                    Id = 7,
                    GadgetId = 3,
                    User = sara
                },
                new FavoriteGadget
                {
                    Id = 8,
                    GadgetId = 6,
                    User = mike
                },
                new FavoriteGadget
                {
                    Id = 9,
                    GadgetId = 6,
                    User = sara
                },
                new FavoriteGadget
                {
                    Id = 10,
                    GadgetId = 6,
                    User = lobna
                }
            });
            context.SaveChanges();
        }
    }
}
