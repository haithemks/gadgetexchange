﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API.Async;

namespace GadgetExchange.Tests.Util.Repository.API.Async
{
    public class MemoryApiFavoriteGadgetRepositoryAsync : IApiFavoriteGadgetRepositoryAsync, IDisposable
    {
        public readonly List<ApplicationUser> MyUsers = new List<ApplicationUser>();
        private readonly List<FavoriteGadget> _myFavoriteGadgets = new List<FavoriteGadget>();

        public MemoryApiFavoriteGadgetRepositoryAsync()
        {
            var mike = new ApplicationUser
            {
                Id = "1",
                Email = "Mike@site.net",
                UserName = "MikeUp"
            };
            var lynda = new ApplicationUser
            {
                Id = "2",
                Email = "Lynda@site.net",
                UserName = "Lynda123"
            };

            MyUsers.AddRange(new List<ApplicationUser>
            {
                mike,
                lynda
            });

            _myFavoriteGadgets.AddRange(new List<FavoriteGadget>
            {
                new FavoriteGadget
                {
                    GadgetId = 1,
                    Id = 1,
                    User = mike
                },
                new FavoriteGadget
                {
                    GadgetId = 2,
                    Id = 2,
                    User = mike
                },
                new FavoriteGadget
                {
                    GadgetId = 3,
                    Id = 3,
                    User = mike
                }
            });
        }

        public async Task<IEnumerable<FavoriteGadget>> GetAllFavoriteGadgets()
        {
            return await Task.Run(() => _myFavoriteGadgets.AsEnumerable());
        }

        public async Task<FavoriteGadget> GetFavoriteGadgetById(int id)
        {
            return await Task.Run(() => _myFavoriteGadgets.FirstOrDefault(x => x.Id == id));
        }

        public async Task AddFavoriteGadget(int gadgetId, string userid)
        {
            await Task.Run(() =>
           {
               var user = MyUsers.FirstOrDefault(x => x.Id == userid);
               _myFavoriteGadgets.Add(new FavoriteGadget
               {
                   GadgetId = gadgetId,
                   User = user
               });
           });
        }

        public async Task<bool> RemoveFavoriteGadget(int gadgetId, string userid)
        {
            if (await FavoriteGadgetExist(gadgetId, userid))
            {
                var favoriteGadget = _myFavoriteGadgets.FirstOrDefault(e => e.GadgetId == gadgetId && e.User.Id == userid);
                Debug.Assert(favoriteGadget != null, nameof(favoriteGadget) + " != null");
                _myFavoriteGadgets.Remove(await GetFavoriteGadgetById(favoriteGadget.Id));
                return true;
            }
            return false;
        }

        public async Task<bool> FavoriteGadgetExist(int id)
        {
            return await Task.Run(() => _myFavoriteGadgets.Count(x => x.Id == id) > 0);
        }

        public async Task<bool> FavoriteGadgetExist(int gadgetId, string userId)
        {
            return await Task.Run(() => _myFavoriteGadgets.Count(x => x.GadgetId == gadgetId && x.User.Id == userId) > 0);
        }

        public void Dispose()
        {
            _myFavoriteGadgets.Clear();
        }

    }
}
