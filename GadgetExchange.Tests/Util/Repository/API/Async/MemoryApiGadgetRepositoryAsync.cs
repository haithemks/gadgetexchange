﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API.Async;

namespace GadgetExchange.Tests.Util.Repository.API.Async
{
    public class MemoryApiGadgetRepsitoryAsync : IApiGadgetRepositoryAsync, IDisposable
    {
        private readonly List<Gadget> _myGadgets = new List<Gadget>();

        public MemoryApiGadgetRepsitoryAsync()
        {
            _myGadgets.AddRange(new List<Gadget>
            {
                new Gadget
                {
                    Id = 1,
                    Name = "IPhone6",
                    ImagePath = "Path6",
                    Description = "IPhone6 Description",
                    ReviewsCount = 3,
                    StatusId = Status.Available,
                    Cost = (float)25.50
                },
                new Gadget
                {
                    Id = 2,
                    Name = "IPhone7",
                    ImagePath = "Path7",
                    Description = "IPhone7 Description",
                    ReviewsCount = 35,
                    StatusId = Status.Available,
                    Cost = (float)10.00
                },
                new Gadget
                {
                    Id = 3,
                    Name = "IPhone8",
                    ImagePath = "Path8",
                    Description = "IPhone8 Description",
                    ReviewsCount = 7,
                    StatusId = Status.Available,
                    Cost = (float)19.99
                },
                new Gadget
                {
                    Id = 4,
                    Name = "Android5",
                    ImagePath = "Path5",
                    Description = "Android5 Description",
                    ReviewsCount = 10,
                    StatusId = Status.Queued,
                    Cost = (float)15.99
                },
                new Gadget
                {
                    Id = 5,
                    Name = "Android4",
                    ImagePath = "Path4",
                    Description = "Android4 Description",
                    ReviewsCount = 13,
                    StatusId = Status.Queued,
                    Cost = (float)25.50
                },
                new Gadget
                {
                    Id = 6,
                    Name = "Tablet1",
                    ImagePath = "Tablet1",
                    Description = "Tablet1 Description",
                    ReviewsCount = 53,
                    StatusId = Status.Mine,
                    Cost = (float)5.99
                },
                new Gadget
                {
                    Id = 7,
                    Name = "Tablet2",
                    ImagePath = "Tablet2",
                    Description = "Tablet2 Description",
                    ReviewsCount = 20,
                    StatusId = Status.Mine,
                    Cost = (float)5.99
                },
                new Gadget
                {
                    Id = 8,
                    Name = "Watch15",
                    ImagePath = "Watch15",
                    Description = "Watch15 Description",
                    ReviewsCount = 10,
                    StatusId = Status.Favorite,
                    Cost = (float)12.99
                }
            });
        }
        public async Task<IEnumerable<Gadget>> GetAllGadgets()
        {
            return await Task.Run(() => _myGadgets.AsEnumerable());
        }
        public async Task<Gadget> GetGadgetById(int id)
        {
            return await Task.Run(() => _myGadgets.FirstOrDefault(x => x.Id == id));
        }
        public async Task AddGadget(Gadget item)
        {
             await Task.Run(() => _myGadgets.Add(item));
        }
        public async Task UpdateGadget(Gadget item)
        {
            var gadget = await Task.Run(() => GetGadgetById(item.Id).Result);
            gadget.Id = item.Id;
            gadget.Cost = item.Cost;
            gadget.Description = item.Description;
            gadget.ImagePath = item.ImagePath;
            gadget.Name = item.Name;
            gadget.ReviewsCount = item.ReviewsCount;
            gadget.StatusId = item.StatusId;
        }
        public async Task RemoveGadget(int id)
        {
            await Task.Run(async () => _myGadgets.Remove(await GetGadgetById(id)));
        }
        public async Task<bool> GadgetExist(int id)
        {
            return await Task.Run(() => _myGadgets.Count(x => x.Id == id) > 0);
        }
        public void Dispose()
        {
            _myGadgets.Clear();
        }

    }
}
