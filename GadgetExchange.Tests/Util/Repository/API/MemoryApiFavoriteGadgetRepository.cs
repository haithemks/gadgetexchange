﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API;

namespace GadgetExchange.Tests.Util.Repository.API
{
    public class MemoryApiFavoriteGadgetRepository : IApiFavoriteGadgetRepository, IDisposable
    {
        public readonly List<ApplicationUser> MyUsers = new List<ApplicationUser>();
        private readonly List<FavoriteGadget> _myFavoriteGadgets = new List<FavoriteGadget>();

        public MemoryApiFavoriteGadgetRepository()
        {
            var mike = new ApplicationUser
            {
                Id = "1",
                Email = "Mike@site.net",
                UserName = "MikeUp"
            };
            var lynda = new ApplicationUser
            {
                Id = "2",
                Email = "Lynda@site.net",
                UserName = "Lynda123"
            };

            MyUsers.AddRange(new List<ApplicationUser>
            {
                mike,
                lynda
            });

            _myFavoriteGadgets.AddRange(new List<FavoriteGadget>
            {
                new FavoriteGadget
                {
                    GadgetId = 1,
                    Id = 1,
                    User = mike
                },
                new FavoriteGadget
                {
                    GadgetId = 2,
                    Id = 2,
                    User = mike
                },
                new FavoriteGadget
                {
                    GadgetId = 3,
                    Id = 3,
                    User = mike
                }
            });
        }

        public IEnumerable<FavoriteGadget> GetAllFavoriteGadgets()
        {
            return _myFavoriteGadgets.AsEnumerable();
        }

        public FavoriteGadget GetFavoriteGadgetById(int id)
        {
            return _myFavoriteGadgets.FirstOrDefault(x => x.Id == id);
        }

        public void AddFavoriteGadget(int gadgetId, string userid)
        {
            var user = MyUsers.FirstOrDefault(x => x.Id == userid);
            _myFavoriteGadgets.Add(new FavoriteGadget
            {
                GadgetId = gadgetId,
                User = user
            });
        }

        public bool RemoveFavoriteGadget(int gadgetId, string userid)
        {
            if (FavoriteGadgetExist(gadgetId, userid))
            {
            var favoriteGadget = _myFavoriteGadgets.FirstOrDefault(e => e.GadgetId == gadgetId && e.User.Id == userid);
            Debug.Assert(favoriteGadget != null, nameof(favoriteGadget) + " != null");
            _myFavoriteGadgets.Remove(GetFavoriteGadgetById(favoriteGadget.Id));
                return true;
            }
            return false;
        }

        public bool FavoriteGadgetExist(int id)
        {
            return _myFavoriteGadgets.Count(x => x.Id == id) > 0;
        }

        public bool FavoriteGadgetExist(int gadgetId, string userId)
        {
            return _myFavoriteGadgets.Count(x => x.GadgetId == gadgetId && x.User.Id == userId) > 0;
        }

        public void Dispose()
        {
            _myFavoriteGadgets.Clear();
        }

    }
}
