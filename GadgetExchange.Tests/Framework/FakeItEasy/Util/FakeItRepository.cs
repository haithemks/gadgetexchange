﻿using FakeItEasy;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API;
using GadgetExchange.Respository.Interface.API.Async;
using GadgetExchange.Respository.Interface.Controllers;
using GadgetExchange.Respository.Interface.Controllers.Async;


namespace GadgetExchange.Tests.Framework.FakeItEasy.Util
{
    public class FakeItRepo
    {
        public IGadgetRepository FakeGadgetRepo(IGadgetRepository memoryRepo)
        {
            var fakeGadgetRepository = A.Fake<IGadgetRepository>();
            A.CallTo(() => fakeGadgetRepository.GetGadgets()).Returns(memoryRepo.GetGadgets());
            A.CallTo(() => fakeGadgetRepository.GetGadgetById(A<int>.Ignored))
                .ReturnsLazily((int id) => memoryRepo.GetGadgetById(id));
            A.CallTo(() => fakeGadgetRepository.FilterGadgetByStatus(A<Status>.Ignored))
                .ReturnsLazily((Status stat) => memoryRepo.FilterGadgetByStatus(stat));
            A.CallTo(() => fakeGadgetRepository.SetRentalPrice(A<int>.Ignored,A<float>.Ignored))
                .Invokes((int id, float price) =>  memoryRepo.SetRentalPrice(id, price));
            A.CallTo(() => fakeGadgetRepository.GetGadgets()).Returns(memoryRepo.GetGadgets());
            A.CallTo(() => fakeGadgetRepository.Save()).Invokes(memoryRepo.Save);
            return fakeGadgetRepository;
        }

        public IGadgetRepositoryAsync FakeGadgetRepoAsync(IGadgetRepositoryAsync memoryRepo)
        {
            var fakeGadgetRepository = A.Fake<IGadgetRepositoryAsync>();
            A.CallTo(() => fakeGadgetRepository.GetGadgets()).Returns(memoryRepo.GetGadgets());
            A.CallTo(() => fakeGadgetRepository.GetGadgetById(A<int>.Ignored))
                .ReturnsLazily((int id) => memoryRepo.GetGadgetById(id));
            A.CallTo(() => fakeGadgetRepository.FilterGadgetByStatus(A<Status>.Ignored))
                .ReturnsLazily((Status stat) => memoryRepo.FilterGadgetByStatus(stat));
            A.CallTo(() => fakeGadgetRepository.SetRentalPrice(A<int>.Ignored, A<float>.Ignored))
                .Invokes((int id, float price) => memoryRepo.SetRentalPrice(id, price));
            A.CallTo(() => fakeGadgetRepository.GetGadgets()).Returns(memoryRepo.GetGadgets());
            A.CallTo(() => fakeGadgetRepository.Save()).Invokes(() => memoryRepo.Save());
            return fakeGadgetRepository;
        }

        public IApiGadgetRepository FakeApiGadgetRepo(IApiGadgetRepository memoryRepo)
        {
            var fakeGadgetRepository = A.Fake<IApiGadgetRepository>();
            A.CallTo(() => fakeGadgetRepository.GetAllGadgets()).Returns(memoryRepo.GetAllGadgets());
            A.CallTo(() => fakeGadgetRepository.GetGadgetById(A<int>.Ignored))
                .ReturnsLazily((int id) => memoryRepo.GetGadgetById(id));
            A.CallTo(() => fakeGadgetRepository.AddGadget(A<Gadget>.Ignored))
                .Invokes((Gadget item) => memoryRepo.AddGadget(item));
            A.CallTo(() => fakeGadgetRepository.UpdateGadget(A<Gadget>.Ignored))
                .Invokes((Gadget item) => memoryRepo.UpdateGadget(item));
            A.CallTo(() => fakeGadgetRepository.RemoveGadget(A<int>.Ignored))
                .Invokes((int id) => memoryRepo.RemoveGadget(id));
            A.CallTo(() => fakeGadgetRepository.GadgetExist(A<int>.Ignored))
                .Invokes((int id) => memoryRepo.GadgetExist(id));
            return fakeGadgetRepository;
        }

        public IApiGadgetRepositoryAsync FakeApiGadgetRepoAsync(IApiGadgetRepositoryAsync memoryRepo)
        {
            var fakeGadgetRepository = A.Fake<IApiGadgetRepositoryAsync>();
            A.CallTo(() => fakeGadgetRepository.GetAllGadgets()).Returns(memoryRepo.GetAllGadgets());
            A.CallTo(() => fakeGadgetRepository.GetGadgetById(A<int>.Ignored))
                .ReturnsLazily((int id) => memoryRepo.GetGadgetById(id));
            A.CallTo(() => fakeGadgetRepository.AddGadget(A<Gadget>.Ignored))
                .Invokes((Gadget item) => memoryRepo.AddGadget(item));
            A.CallTo(() => fakeGadgetRepository.UpdateGadget(A<Gadget>.Ignored))
                .Invokes((Gadget item) => memoryRepo.UpdateGadget(item));
            A.CallTo(() => fakeGadgetRepository.RemoveGadget(A<int>.Ignored))
                .Invokes((int id) => memoryRepo.RemoveGadget(id));
            A.CallTo(() => fakeGadgetRepository.GadgetExist(A<int>.Ignored))
                .Invokes((int id) => memoryRepo.GadgetExist(id));
            return fakeGadgetRepository;
        }
    }
}