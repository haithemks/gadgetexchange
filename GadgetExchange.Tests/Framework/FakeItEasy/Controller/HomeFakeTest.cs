﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using GadgetExchange.Controllers;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.Controllers;
using GadgetExchange.Tests.Framework.FakeItEasy.Util;
using GadgetExchange.Tests.Util.Repository.Controller;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GadgetExchange.Tests.Framework.FakeItEasy.Controller
{
    [TestClass]
    public class HomeFakeTest
    {
        private HomeController _homeController;

        [TestInitialize]
        public void Initialize()
        {
            // Arrange
            IGadgetRepository memoryRepo = new MemoryGadgetRepsitory();

            IGadgetRepository fakeGadgetRepository = new FakeItRepo().FakeGadgetRepo(memoryRepo);

            _homeController = new HomeController(fakeGadgetRepository);

        }

        [TestCategory("Index")]

        [TestMethod]
        public void Index_Where_Page_is_Null_And_Filter_isNull_Should_Return_Null()
        {
            // Act
            ViewResult result = _homeController.Index(null) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Index_Where_Page_is_One_And_Filter_isNull_Should_Return_Nine_Gadget()
        {
            // Act
            ViewResult result = _homeController.Index(1) as ViewResult;

            Debug.Assert(result != null, nameof(result) + " != null");

            IEnumerable<Gadget> model = result.Model as IEnumerable<Gadget>;

            // Assert
            if (model != null) Assert.AreEqual(8, model.Count());
        }

        [TestMethod]
        public void Index_Where_Page_is_One_And_Filter_is_Mine_Should_Return_My_Gadget()
        {
            // Act
            ViewResult result = _homeController.Index(1, Status.Mine.ToString()) as ViewResult;

            Debug.Assert(result != null, nameof(result) + " != null");

            IEnumerable<Gadget> model = result.Model as IEnumerable<Gadget>;

            Debug.Assert(model != null, nameof(model) + " != null");

            Assert.AreEqual(2, model.Count());
        }

        [TestMethod]
        public void ChildAction_StatusSelection_Should_Return_AllStatus()
        {
            // Act
            ViewResult result = _homeController.StatusSelection() as ViewResult;

            if (result != null)
            {
                IEnumerable<Statu> model = result.Model as IEnumerable<Statu>;

                // Assert
                if (model != null) Assert.AreEqual(5, model.Count());
            }
        }

        [TestCleanup]
        public void CleanUp()
        {
            _homeController.Dispose();
        }
    }
}
