﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Xunit;

namespace GadgetExchange.Tests.Framework.Selenium.Controllers.Async
{

    public class GadgetsUiTest : IDisposable
    {
        private IWebDriver _driver;
        private string _baseUrl = "http://haithem-araissia.com/gadgetexchange/gadgets/";
        private int _milliSecondsDelay = 1000;
        private int _implicitWaitDelay = 10;

        [Fact]
        public void RunGadgetsPage()
        {
            _driver = new ChromeDriver();

            _driver.Manage().Window.Maximize();

            Pagination();

            NotAuthenticatedActions();

            Login();

            AuthenticatedActions();
        }

        #region Actions

        private void NotAuthenticatedActions()
        {
            NotAuthenticatedAddToFavorite();
            NotAuthenticatedRemovedFromFavorite();
        }

        private void AuthenticatedActions()
        {
            AuthenticatedAddToFavorite();
            AuthenticatedRemovedFromFavorite();
        }

        #endregion

        #region Commands

        #region NotAuthenticated

        private void NotAuthenticatedAddToFavorite()
        {
            AddToFavoriteClick();

            Assert.Contains("/gadgetexchange/Account/Login?ReturnUrl=home", _driver.Url);
        }

        private void NotAuthenticatedRemovedFromFavorite()
        {
            RemoveFromFavoriteClick();

            Assert.Contains("/gadgetexchange/Account/Login?ReturnUrl=home", _driver.Url);
        }

        #endregion

        #region Authenticated

        private void AuthenticatedAddToFavorite()
        {
            AddToFavoriteClick();

            IWebElement confirmationMessage = _driver.FindElement(By.CssSelector("p.lead.text-muted"));

            Assert.Contains("Gadget added to Favorite.", confirmationMessage.Text);

            IWebElement okButton = _driver.FindElement(By.CssSelector("button.confirm.btn.btn-lg.btn-primary"));

            okButton.Click();
        }
        private void AuthenticatedRemovedFromFavorite()
        {
            RemoveFromFavoriteClick();

            IWebElement confirmationMessage = _driver.FindElement(By.CssSelector("p.lead.text-muted"));

            Assert.Contains("Gadget removed from Favorite.", confirmationMessage.Text);

            IWebElement okButton = _driver.FindElement(By.CssSelector("button.confirm.btn.btn-lg.btn-primary"));

            okButton.Click();
        }

        #endregion

        #endregion

        #region Clicks

        private void Login()
        {
            if (!_driver.Url.Contains("gadgetexchange/Account/Login"))
            {
                _driver.Navigate().GoToUrl(_baseUrl + "Account/Login");
            }

            IWebElement emailInput = _driver.FindElement(By.Id("Email"));

            emailInput.SendKeys("Selenium@gadgetExchange.com");

            IWebElement passwordInput = _driver.FindElement(By.Id("Password"));

            passwordInput.SendKeys("Selenium$123");

            IWebElement loginButton = _driver.FindElement(By.Id("Login"));

            loginButton.Click();

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(_implicitWaitDelay);

            Task.Delay(_milliSecondsDelay).Wait();

            IWebElement toogleButton = _driver.FindElement(By.CssSelector(".dropdown-toggle"));

            toogleButton.Click();

            IWebElement greeting = _driver.FindElement(By.Id("Greeting"));

            Assert.Equal("Hello Selenium@gadgetExchange.com!", greeting.Text);
        }

        private void AddToFavoriteClick()
        {
            _driver.Navigate().GoToUrl(_baseUrl);

            IList<IWebElement> allNotFavoriteItems = _driver.FindElements(By.CssSelector(".fa-heart-not"));

            allNotFavoriteItems[0].Click();

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(_implicitWaitDelay);

            Task.Delay(_milliSecondsDelay).Wait();
        }

        private void RemoveFromFavoriteClick()
        {
            _driver.Navigate().GoToUrl(_baseUrl);

            IList<IWebElement> allFavoriteItems = _driver.FindElements(By.CssSelector(".fa-heart"));

            allFavoriteItems[0].Click();

            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(_implicitWaitDelay);

            Task.Delay(_milliSecondsDelay).Wait();
        }

        private void Pagination()
        {
            _driver.Navigate().GoToUrl(_baseUrl);

            IList<IWebElement> allPagingLinks = _driver.FindElements(By.CssSelector(".pagination li a"));

            var thirdPageLink = allPagingLinks[3];

            thirdPageLink.Click();

            IList<IWebElement> gadgetItemCount = _driver.FindElements(By.CssSelector(".product-list-basic li"));

            Assert.True(gadgetItemCount.Count > 1);
        }

        #endregion

        public void Dispose()
        {
            _driver.Quit();

            _driver.Dispose();
        }
    }
}
