﻿using System;
using OpenQA.Selenium;

namespace GadgetExchange.Tests.Framework.Selenium.Util
{
    public class Pattern : IDisposable
    {
        private IWebDriver _driver;

        public void StartApplication()
        {  
            #region Pattern

            //-------------------Starting the Application-------------------//
            //_driver = new ChromeDriver();

            //_driver.Manage().Window.Maximize();

            //_driver.Navigate().GoToUrl("https://localhost:44382/");

            //-------------------Getting Element And Clicks-------------------//

            //_driver.FindElement(By.Id("Test123")).Click();

            //_driver.FindElement(By.CssSelector(".btn.btn-primary")).Click();

            //---Span Confirmation---//
            //IWebElement confirmationNamespan = _driver.FindElement(By.Id("Loan"));

            //string confirmationSpan = confirmationNamespan.Text;

            //Assert.Equal("sarah", confirmationSpan);

            //-----------------------Click Confirmation-----------------------//

            // IWebElement applicationButton = driver.FindElement(By.Id("buttonId"));

            // applicationButton.Click();

            //  Assert.Equal("", driver.Title);

            //----------------------------Key Strocks-------------------------//

            // IWebElement firstNameInput = driver.FindElement(By.Id("FirstName"));

            // firstNameInput.SendKeys("Sarah");


            //------------------------------TimeOut---------------------------//

            //_driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));

            //WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(5));
            //wait.Until(d =>
            //{
            //    var elements = _driver.FindElements(By.ClassName("k1"));
            //    if (elements.Count > 0)
            //        return elements[0];
            //    return null;
            //});   

            #endregion
        }

        public void Dispose()
        {
            _driver.Quit();

            _driver.Dispose();
        }
    }
}
