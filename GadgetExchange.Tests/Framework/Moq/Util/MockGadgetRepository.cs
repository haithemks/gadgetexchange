﻿using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API;
using GadgetExchange.Respository.Interface.API.Async;
using GadgetExchange.Respository.Interface.Controllers;
using GadgetExchange.Respository.Interface.Controllers.Async;
using Moq;

namespace GadgetExchange.Tests.Framework.Moq.Util
{
    public class MoqRepo
    {
        public Mock<IGadgetRepository> MoqGadgetRepo(IGadgetRepository memoryRepo)
        {
            Mock<IGadgetRepository> mockGadgetRepository = new Mock<IGadgetRepository>();
            mockGadgetRepository.Setup(x => x.GetGadgets()).Returns(memoryRepo.GetGadgets());
            mockGadgetRepository.Setup(x => x.GetGadgetById(It.IsAny<int>()))
                .Returns((int id) => memoryRepo.GetGadgetById(id));
            mockGadgetRepository.Setup(x => x.FilterGadgetByStatus(It.IsAny<Status>()))
                .Returns((Status stat) => memoryRepo.FilterGadgetByStatus(stat));
            mockGadgetRepository.Setup(x => x.SetRentalPrice(It.IsAny<int>(), It.IsAny<float>())).Callback
                ((int id, float price) => memoryRepo.SetRentalPrice(id, price));
            mockGadgetRepository.Setup(x => x.Save()).Callback(() => memoryRepo.Save());
            return mockGadgetRepository;
        }


        public Mock<IGadgetRepositoryAsync> MoqGadgetRepoAsync(IGadgetRepositoryAsync memoryRepo)
        {
            Mock<IGadgetRepositoryAsync> mockGadgetRepository = new Mock<IGadgetRepositoryAsync>();
            mockGadgetRepository.Setup(x => x.GetGadgets()).Returns(memoryRepo.GetGadgets());
            mockGadgetRepository.Setup(x => x.GetGadgetById(It.IsAny<int>()))
                .Returns((int id) => memoryRepo.GetGadgetById(id));
            mockGadgetRepository.Setup(x => x.FilterGadgetByStatus(It.IsAny<Status>()))
                .Returns((Status stat) => memoryRepo.FilterGadgetByStatus(stat));
            mockGadgetRepository.Setup(x => x.SetRentalPrice(It.IsAny<int>(), It.IsAny<float>())).Callback
                ((int id, float price) => memoryRepo.SetRentalPrice(id, price));
            mockGadgetRepository.Setup(x => x.Save()).Callback(() => memoryRepo.Save());
            return mockGadgetRepository;
        }

        public Mock<IApiGadgetRepository> MoqApiGadgetRepo(IApiGadgetRepository memoryRepo)
        {
            Mock<IApiGadgetRepository> mockGadgetRepository = new Mock<IApiGadgetRepository>();
            mockGadgetRepository.Setup(x => x.GetAllGadgets()).Returns(memoryRepo.GetAllGadgets());
            mockGadgetRepository.Setup(x => x.GetGadgetById(It.IsAny<int>()))
                .Returns((int id) => memoryRepo.GetGadgetById(id));
            mockGadgetRepository.Setup(x => x.AddGadget(It.IsAny<Gadget>())).
                Callback((Gadget item) => memoryRepo.AddGadget(item));
            mockGadgetRepository.Setup(x => x.UpdateGadget(It.IsAny<Gadget>())).
                Callback((Gadget item) => memoryRepo.UpdateGadget(item));
            mockGadgetRepository.Setup(x => x.RemoveGadget(It.IsAny<int>())).
                Callback((int id) => memoryRepo.RemoveGadget(id));
            mockGadgetRepository.Setup(x => x.GadgetExist(It.IsAny<int>())).
                Callback((int id) => memoryRepo.GadgetExist(id));
            return mockGadgetRepository;
        }

        public Mock<IApiGadgetRepositoryAsync> MoqApiGadgetRepoAsync(IApiGadgetRepositoryAsync memoryRepo)
        {
            Mock<IApiGadgetRepositoryAsync> mockGadgetRepository = new Mock<IApiGadgetRepositoryAsync>();
            mockGadgetRepository.Setup(x => x.GetAllGadgets()).Returns(memoryRepo.GetAllGadgets());
            mockGadgetRepository.Setup(x => x.GetGadgetById(It.IsAny<int>()))
                .Returns((int id) => memoryRepo.GetGadgetById(id));
            mockGadgetRepository.Setup(x => x.AddGadget(It.IsAny<Gadget>())).
                Callback((Gadget item) => memoryRepo.AddGadget(item));
            mockGadgetRepository.Setup(x => x.UpdateGadget(It.IsAny<Gadget>())).
                Callback((Gadget item) => memoryRepo.UpdateGadget(item));
            mockGadgetRepository.Setup(x => x.RemoveGadget(It.IsAny<int>())).
                Callback((int id) => memoryRepo.RemoveGadget(id));
            mockGadgetRepository.Setup(x => x.GadgetExist(It.IsAny<int>())).
                Callback((int id) => memoryRepo.GadgetExist(id));
            return mockGadgetRepository;
        }
    }
}