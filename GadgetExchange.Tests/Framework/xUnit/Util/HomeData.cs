﻿using System.Collections;
using System.Collections.Generic;
using GadgetExchange.Models.Extension;

namespace GadgetExchange.Tests.Framework.xUnit.Util
{

    public class HomeData : IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
            new object[] { "1", Status.Available.ToString() },
            new object[] { "1", Status.All.ToString() },
            new object[] { "1", Status.Favorite.ToString() },
            new object[] { "1", Status.Mine.ToString() },
            new object[] { "1", Status.Queued.ToString() }
        };

        public IEnumerator<object[]> GetEnumerator()
        { return _data.GetEnumerator(); }

        IEnumerator IEnumerable.GetEnumerator()
        { return GetEnumerator(); }
    }
}
