﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Http.Routing;
using GadgetExchange.API;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API;
using GadgetExchange.Tests.Util.Repository.API;
using Xunit;

namespace GadgetExchange.Tests.Framework.xUnit.API
{
    public class ApiGadgetxUnitTest : IDisposable
    {
        private readonly GadgetController _gadgetController;

        public ApiGadgetxUnitTest()
        {
            // Arrange
            IApiGadgetRepository repo = new MemoryApiGadgetRepsitory();

            _gadgetController = new GadgetController(repo);
        }

        [Fact]
        [Trait("ApiGadget", "CRUD")]
        public void GetAllGadgets_Should_Return_AllGadgets()
        {
            // Act
            var response = _gadgetController.GetGadgets();

            // Assert
            Assert.Equal(8, response.Count());
        }

        [Fact]
        [Trait("ApiGadget", "CRUD")]
        public void GetGadgetById_Should_Return_NotFound_When_GadgetDoesNotExist()
        {
            // Act
            IHttpActionResult actionResultNotFound = _gadgetController.GetGadget(20);

            // Assert
            Assert.IsType<NotFoundResult>(actionResultNotFound);
        }

        [Theory]
        [Trait("ApiGadget", "CRUD")]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void GetGadgetById_Should_Return_CorrectGadget_When_GadgetExist(int id)
        {
            // Act
            var actionResult = _gadgetController.GetGadget(id);
            var contentResult = actionResult as OkNegotiatedContentResult<Gadget>;

            // Assert
            Assert.NotNull(contentResult);
            Assert.NotNull(contentResult.Content);
            Assert.Equal(id, contentResult.Content.Id);
        }

        [Fact]
        [Trait("ApiGadget", "CRUD")]
        public void PutGadget_Should_UpdateGadget_If_GadgetExist()
        {
            // Act
            var newGadget = new Gadget
            {
                Id = 2,
                Name = "IPhone7",
                ImagePath = "Path7",
                Description = "new Description",
                ReviewsCount = 35,
                StatusId = Status.Available,
                Cost = (float)10.00
            };

            var actionResult = _gadgetController.PutGadget(2, newGadget);
            var contentResult = actionResult as NegotiatedContentResult<Gadget>;

            // Assert
            Assert.NotNull(contentResult);
            Assert.Equal(HttpStatusCode.Accepted, contentResult.StatusCode);
            Assert.Equal("new Description", contentResult.Content.Description);
            Assert.NotNull(contentResult.Content);
        }

        [Fact]
        [Trait("ApiGadget", "CRUD")]
        public void PostGadget_Should_AddGadget_ToListofGadgets()
        {

            // SetUp
            var newGadget = new Gadget
            {
                Id = 10,
                Name = "IPhone10",
                ImagePath = "Path10",
                Description = "Description 10",
                ReviewsCount = 10,
                StatusId = Status.Available,
                Cost = (float)10.00
            };

            var requestUrl = new Uri("https://localhost:44382/api/gadget");
            _gadgetController.Request = new HttpRequestMessage
            {
                RequestUri = requestUrl
            };
            _gadgetController.Configuration = new HttpConfiguration();
            _gadgetController.Configuration.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            _gadgetController.RequestContext.RouteData = new HttpRouteData(
                route: new HttpRoute(),
                values: new HttpRouteValueDictionary { { "controller", "gadget" } });

            // Act
            var actionResult = _gadgetController.PostGadget(newGadget);
            var createdResult = actionResult as CreatedNegotiatedContentResult<Gadget>;

            // Assert  
            Assert.NotNull(createdResult);
            Assert.Equal(10, createdResult.Content.Id);
            Assert.Equal(requestUrl + "/10", createdResult.Location.AbsoluteUri);

        }

        [Fact]
        [Trait("ApiGadget", "CRUD")]
        public void DeleteGadget_Should_DeleteGadget_If_GadgetExist()
        {
            var actionResult = _gadgetController.DeleteGadget(2);
            var contentResult = actionResult as OkNegotiatedContentResult<Gadget>;

            // Assert
            Assert.NotNull(contentResult);
            Assert.NotNull(contentResult.Content);
            Assert.Equal(2, contentResult.Content.Id);

            // Act
            IHttpActionResult actionResultNotFound = _gadgetController.DeleteGadget(20);

            // Assert
            Assert.IsType<NotFoundResult>(actionResultNotFound);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _gadgetController?.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
