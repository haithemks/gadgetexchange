﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using GadgetExchange.Controllers;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.Controllers.Async;
using GadgetExchange.Tests.Framework.xUnit.Util;
using GadgetExchange.Tests.Util.Repository.Controller.Async;
using Xunit;

namespace GadgetExchange.Tests.Framework.xUnit.Controllers.Async
{
    public class GadgetsxUnitTest : IDisposable
    {
        private readonly GadgetsController _gadgetsController;

        public GadgetsxUnitTest()
        {
            // Arrange
            IGadgetRepositoryAsync repo = new MemoryGadgetRepsitoryAsync();

            _gadgetsController = new GadgetsController(repo);
        }

        [Fact]
        [Trait("GadgetsController", "Index")]
        public void Index_Where_Page_is_Null_And_Filter_isNull_Should_Return_Null()
        {

            // Act
            var result = _gadgetsController.Index(null);

            var viewresult = result.Result;

            // Assert
            Assert.NotNull(viewresult);
        }

        [Theory]
        [Trait("GadgetsController", "Index")]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void Index_Where_Page_is_One_And_Filter_isNull_Should_Return_Nine_Gadget(int pageId)
        {
            // Act
            var result = _gadgetsController.Index(pageId);

            var viewresult = result.Result as ViewResult;

            Debug.Assert(viewresult != null, nameof(viewresult) + " != null");

            IEnumerable<Gadget> model = viewresult.Model as IEnumerable<Gadget>;

            // Assert
            Debug.Assert(model != null, nameof(model) + " != null");
            Assert.True(((PagedList.PagedListMetaData)(model)).TotalItemCount > 0);
        }

        [Theory]
        [Trait("HomeController", "Index")]
        [ClassData(typeof(HomeData))]
        public async Task Index_Where_Page_is_One_And_Filter_is_Mine_Should_Return_My_Gadget(int page, string filter)
        {
            var result = _gadgetsController.Index(page, filter);

            Assert.NotNull(result);

            var viewresult = await result as ViewResult;

            Debug.Assert(viewresult != null, nameof(viewresult) + " != null");

            IEnumerable<Gadget> model = viewresult.Model as IEnumerable<Gadget>;

            Debug.Assert(model != null, nameof(model) + " != null");

            Assert.True(((PagedList.PagedListMetaData)(model)).TotalItemCount > 0);
        }

        [Fact]
        [Trait("HomeController", "ChildAction")]
        public void ChildAction_StatusSelection_Should_Return_AllStatus()
        {
            // Act
            ViewResult result = _gadgetsController.StatusSelection() as ViewResult;

            if (result != null)
            {
                IEnumerable<Statu> model = result.Model as IEnumerable<Statu>;

                // Assert
                if (model != null) Assert.Equal(5, model.Count());
            }
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _gadgetsController?.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
