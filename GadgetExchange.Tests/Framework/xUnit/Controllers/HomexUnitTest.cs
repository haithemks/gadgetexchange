﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using GadgetExchange.Controllers;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.Controllers;
using GadgetExchange.Tests.Framework.xUnit.Util;
using GadgetExchange.Tests.Util.Repository.Controller;
using Xunit;
namespace GadgetExchange.Tests.Framework.xUnit.Controllers
{
    public class HomexUnitTest : IDisposable
    {
        private readonly HomeController _homeController;

        public HomexUnitTest()
        {
            // Arrange
            IGadgetRepository repo = new MemoryGadgetRepsitory();

            _homeController = new HomeController(repo);

        }
        [Fact]
        [Trait("HomeController","Index")]
        public void Index_Where_Page_is_Null_And_Filter_isNull_Should_Return_Null()
        {

            // Act
            ViewResult result = _homeController.Index(null) as ViewResult;

            // Assert
            Assert.NotNull(result);
        }

        [Theory]
        [Trait("HomeController", "Index")]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void Index_Where_Page_is_One_And_Filter_isNull_Should_Return_Nine_Gadget(int pageId)
        {
            // Act
            ViewResult result = _homeController.Index(pageId) as ViewResult;

            Debug.Assert(result != null, nameof(result) + " != null");

            IEnumerable<Gadget> model = result.Model as IEnumerable<Gadget>;

            // Assert
            Debug.Assert(model != null, nameof(model) + " != null");
            Assert.True(((PagedList.PagedListMetaData)(model)).TotalItemCount > 0);
        }

        [Theory]
        [Trait("HomeController", "Index")]
        [ClassData(typeof(HomeData))]
        public void Index_Where_Page_is_One_And_Filter_is_Mine_Should_Return_My_Gadget(int page, string filter)
        {

            // Act
            ViewResult result = _homeController.Index(page, filter) as ViewResult;

            Debug.Assert(result != null, nameof(result) + " != null");

            IEnumerable<Gadget> model = result.Model as IEnumerable<Gadget>;

            Debug.Assert(model != null, nameof(model) + " != null");

            Assert.True(((PagedList.PagedListMetaData)(model)).TotalItemCount > 0);
        }

        [Fact]
        [Trait("HomeController", "ChildAction")]
        public void ChildAction_StatusSelection_Should_Return_AllStatus()
        {
            // Act
            ViewResult result = _homeController.StatusSelection() as ViewResult;

            if (result != null)
            {
                IEnumerable<Statu> model = result.Model as IEnumerable<Statu>;

                // Assert
                if (model != null) Assert.Equal(5, model.Count());
            }
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _homeController?.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
