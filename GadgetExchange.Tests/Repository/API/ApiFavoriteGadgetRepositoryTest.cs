﻿using System.Diagnostics;
using System.Linq;
using Effort;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Base.API;
using GadgetExchange.Respository.Interface.API;
using GadgetExchange.Tests.Util.Repository.Seed;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GadgetExchange.Tests.Repository.API
{
    [TestClass]
    public class ApiFavoriteGadgetRepositoryTest
    {
        private ApplicationDbContext _context;
        private IApiFavoriteGadgetRepository _repository;
        private SeedHelper _seedHelper;

        [TestInitialize]
        public void Initialize()
        {
            Effort.Provider.EffortProviderConfiguration.RegisterProvider();
            var connection = DbConnectionFactory.CreateTransient();
            _context = new ApplicationDbContext(connection);
            _seedHelper = new SeedHelper(_context);
            _repository = new ApiFavoriteGadgetRepository(_context);
        }

        [TestMethod]
        public void GetAllFavoriteGadgets_Should_Return_AllFavoriteGadgets()
        {
            // Act
            var gadgets = _repository.GetAllFavoriteGadgets();

            // Assert
            Assert.IsNotNull(gadgets);
            Assert.AreEqual(gadgets.AsQueryable().Count(), 10);
        }

        [TestMethod]
        public void GetFavoriteGadgetByGadgetId_WitExistingId_Should_Return_Gadget()
        {
            // Arrange
            const int existingId = 5;

            // Act
            var favoriteGadget = _repository.GetFavoriteGadgetById(existingId);

            // Assert
            Assert.IsNotNull(favoriteGadget);
            Assert.AreEqual(favoriteGadget.Id, 5);
        }

        [TestMethod]
        public void GetFavoriteGadgetByGadgetId_WithNonExistingId_Should_Return_Null()
        {
            // Arrange
            const int nonExistingId = 155;

            // Act
            var gadget = _repository.GetFavoriteGadgetById(nonExistingId);

            // Assert
            Assert.IsNull(gadget);
        }

        [TestMethod]
        public void AddFavoriteGadget_Should_AddFavoriteGadgetToListofGadgets()
        {
            // Act
            var favoritegadgetsCount = _repository.GetAllFavoriteGadgets().Count();
            var expectedGadgetCount = favoritegadgetsCount + 1;
            var gadget = _context.Gadgets.Find(3);
            Debug.Assert(gadget != null, nameof(gadget) + " != null");
            var gadgetStatus = gadget.StatusId;
            Assert.AreNotEqual(gadgetStatus, Status.Favorite);

            _repository.AddFavoriteGadget(gadget.Id, "1");
            var allFavoriteGadgets = _repository.GetAllFavoriteGadgets().ToList();
            var newFavoriteGadget = allFavoriteGadgets.First(x => x.GadgetId == 3 && x.User.Id == "1");
           
            // Assert
            Assert.IsNotNull(newFavoriteGadget);
            Assert.AreEqual(gadget.StatusId, Status.Favorite);
            Assert.AreEqual(newFavoriteGadget.User.UserName,"Mike");
            Assert.AreEqual(allFavoriteGadgets.Count, expectedGadgetCount);
        }

        [TestMethod]
        public void RemoveFavoriteGadget_Should_DeleteFavoriteGadget_From_ListofFavoriteGadgets()
        {
            // Act
            var favoritegadgetsCount = _repository.GetAllFavoriteGadgets().Count();
            var expectedGadgetCount = favoritegadgetsCount - 1;
            var favoriteGadget = _repository.GetFavoriteGadgetById(4);
            var favoriteGadgetId = favoriteGadget.Id;
            Assert.IsNotNull(favoriteGadget);
            Assert.AreEqual(favoriteGadget.GadgetId, 5);
            Assert.AreEqual(favoriteGadget.User.UserName, "Sara");

            _repository.RemoveFavoriteGadget(favoriteGadget.GadgetId, favoriteGadget.User.Id);
            var allFavoriteGadgets = _repository.GetAllFavoriteGadgets().ToList();
            var removedGaget = _repository.GetFavoriteGadgetById(favoriteGadgetId);
            var gadget = _context.Gadgets.Find(favoriteGadget.GadgetId);

            // Assert
            Assert.IsNull(removedGaget);
            Assert.AreEqual(allFavoriteGadgets.Count, expectedGadgetCount);
            Debug.Assert(gadget != null, nameof(gadget) + " != null");
            Assert.AreEqual(gadget.StatusId, Status.Available);
        }

        [TestMethod]
        public void FavoriteGadgetExist_Should_Returns_True_If_FavoriteGadgetExist()
        {
            // Act
            var id = 1;
            var gadgetId = 6;
            var userId = 3;

            var gadgetExist = _repository.FavoriteGadgetExist(id);
            var gadgetExistByGadgetIdAndUserId = _repository.FavoriteGadgetExist(gadgetId, userId.ToString());

            // Assert
            Assert.IsTrue(gadgetExist);
            Assert.IsTrue(gadgetExistByGadgetIdAndUserId);
        }

        [TestMethod]
        public void FavoriteGadgetExist_Should_Returns_False_If_FavoriteGadgetDoesNotExist()
        {
            // Act
            var id = 122;
            var gadgetId = 122;
            var userId = 5;

            var gadgetDoesNotExist = _repository.FavoriteGadgetExist(id);
            var gadgetDoesNotExistByGadgetIdAndUserId = _repository.FavoriteGadgetExist(gadgetId, userId.ToString());

            // Assert
            Assert.IsFalse(gadgetDoesNotExist);
            Assert.IsFalse(gadgetDoesNotExistByGadgetIdAndUserId);
        }
    }
}
