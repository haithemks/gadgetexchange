﻿using System.Linq;
using System.Threading.Tasks;
using Effort;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Base.API.Async;
using GadgetExchange.Tests.Util.Repository.Seed;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GadgetExchange.Tests.Repository.API.Async
{
    [TestClass]
    public class ApiGadgetRepositoryAsyncTest
    {

        private ApplicationDbContext _context;
        private ApiGadgetRepositoryAsync _repository;
        private SeedHelper _seedHelper;

        [TestInitialize]
        public void Initialize()
        {
            Effort.Provider.EffortProviderConfiguration.RegisterProvider();
            var connection = DbConnectionFactory.CreateTransient();
            _context = new ApplicationDbContext(connection);
            _seedHelper = new SeedHelper(_context);
            _repository = new ApiGadgetRepositoryAsync(_context);
        }

        [TestMethod]
        public void GetAllGadgets_Should_Return_AllGadgets()
        {
            // Act
            var gadgets = _repository.GetAllGadgets();

            // Assert
            Assert.IsNotNull(gadgets);
            Assert.AreEqual(gadgets.Result.AsQueryable().Count(), 8);
        }

        [TestMethod]
        public void GetGadgetById_WitExistingId_Should_Return_Gadget()
        {
            // Arrange
            const int existingId = 5;

            // Act
            var gadget = _repository.GetGadgetById(existingId);

            // Assert
           Assert.IsNotNull(gadget);
           Assert.AreEqual(gadget.Result.ReviewsCount, 13);
        }

        [TestMethod]
        public void GetGadgetById_WithNonExistingId_Should_Return_Null()
        {
            // Arrange
            const int nonExistingId = 155;

            // Act
            var gadget = _repository.GetGadgetById(nonExistingId);

            // Assert
            Assert.IsNull(gadget.Result);
        }

        [TestMethod]
        public async Task AddGadget_Should_AddGadgetToListofGadgets()
        {
            // Act
            var gadgetsCount = _repository.GetAllGadgets().Result.Count();
            var expectedGadgetCount = gadgetsCount + 1;
            var newGadget = new Gadget
            {
                Name = "IPhone12",
                ImagePath = "Path12",
                Description = "IPhone12 Description",
                ReviewsCount = 0,
                StatusId = Status.Available,
                Cost = (float) 22.00
            };
            await _repository.AddGadget(newGadget);
            var gadgets = _repository.GetAllGadgets();

            // Assert
            Assert.IsNotNull(gadgets);
            Assert.AreEqual(gadgets.Result.AsQueryable().Count(), expectedGadgetCount);
        }

        [TestMethod]
        public async Task UpdateGadget_Should_UpdateGadget()
        {
            // Act
            var gadget = _repository.GetGadgetById(3).Result;
            gadget.Description = "Updated Description";
            gadget.StatusId = Status.Favorite;
            await _repository.UpdateGadget(gadget);
            var updatedGadget = _repository.GetGadgetById(3);

            // Assert
            Assert.IsNotNull(updatedGadget.Result);
            Assert.AreEqual(updatedGadget.Result.Description, "Updated Description");
            Assert.AreEqual(updatedGadget.Result.StatusId, Status.Favorite);
        }

        [TestMethod]
        public async Task RemoveGadget_Should_DeleteGadget_From_ListofGadgets()
        {
            // Act
            var gadget = _repository.GetGadgetById(4).Result;
            Assert.IsNotNull(gadget);
            var gadgets = _repository.GetAllGadgets().Result.AsQueryable().Count();
            var expectedCount = gadgets - 1;
            await _repository.RemoveGadget(gadget.Id);
            var removedGadget = _repository.GetGadgetById(4).Result;
            
            // Assert 
            Assert.AreEqual(_repository.GetAllGadgets().Result.AsQueryable().Count(), expectedCount);
            Assert.IsNull(removedGadget);
        }

        [TestMethod]
        public void GadgetExist_Should_Returns_True_IfGadgetExist()
        {
            // Act
            var gadgetExist = _repository.GadgetExist(5);

            // Assert
            Assert.IsTrue(gadgetExist.Result);
        }

        [TestMethod]
        public void GadgetExist_Should_Returns_False_IfGadgetDoesNotExist()
        {
            // Act
            var gadgetDoesNotExist = _repository.GadgetExist(125);

            // Assert
            Assert.IsFalse(gadgetDoesNotExist.Result);
        }
    }
}
