﻿using System.Linq;
using Effort;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Base.Controllers;
using GadgetExchange.Respository.Interface.Controllers;
using GadgetExchange.Tests.Util.Repository.Seed;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GadgetExchange.Tests.Repository.Controllers
{
    [TestClass]
    public class GadgetRepositoryTest
    {

        private ApplicationDbContext _context;
        private IGadgetRepository _repository;
        private SeedHelper _seedHelper;

        [TestInitialize]
        public void Initialize()
        {
            Effort.Provider.EffortProviderConfiguration.RegisterProvider();
            var connection = DbConnectionFactory.CreateTransient();
            _context = new ApplicationDbContext(connection);
            _seedHelper = new SeedHelper(_context);
            _repository = new GadgetRepository(_context);
        }

        [TestMethod]
        public void GetGadgets_Should_Return_AllGadgets()
        {
            // Act
            var gadgets = _repository.GetGadgets();

            // Assert
            Assert.IsNotNull(gadgets);
            Assert.AreEqual(gadgets.AsQueryable().Count(), 8);
        }

        [TestMethod]
        public void FilterGadgetByStatus_Should_Return_AllGadgetsWithStatus()
        {
            // Act
            var gadgets = _repository.FilterGadgetByStatus(Status.Available);

            // Assert
            Assert.IsNotNull(gadgets);
            Assert.AreEqual(gadgets.AsQueryable().Count(), 3);
        }

        [TestMethod]
        public void GetGadgetById_WitExistingId_Should_Return_Gadget()
        {
            // Arrange
            const int existingId = 5;

            // Act
            var gadget = _repository.GetGadgetById(existingId);

            // Assert
           Assert.IsNotNull(gadget);
           Assert.AreEqual(gadget.ReviewsCount, 13);
        }


        [TestMethod]
        public void GetGadgetById_WithNonExistingId_Should_Return_Null()
        {
            // Arrange
            const int nonExistingId = 155;

            // Act
            var gadget = _repository.GetGadgetById(nonExistingId);

            // Assert
            Assert.IsNull(gadget);
        }


        [TestMethod]
        public void SetRentalPrice_ShouldSetPriceOfGadget()
        {
            // Act
            _repository.SetRentalPrice(2,(float) 15.55);

            // Assert
            // Act
            var gadget = _repository.GetGadgetById(2);

            // Assert
            Assert.IsNotNull(gadget);
            Assert.AreEqual(gadget.Cost, (float)15.55);
        }

    }
}
