﻿using System;
using System.Collections.Generic;
using GadgetExchange.Models.Extension;

namespace GadgetExchange.Respository.Interface.Controllers
{
    public interface IGadgetRepository : IDisposable
    {
        IEnumerable<Gadget> GetGadgets();

        IEnumerable<Gadget> FilterGadgetByStatus(Status status);

        Gadget GetGadgetById(int id);

        void SetRentalPrice(int id, float price);

        void Save();
    }
}
