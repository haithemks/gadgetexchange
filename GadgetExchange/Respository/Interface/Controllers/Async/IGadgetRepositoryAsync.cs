﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GadgetExchange.Models.Extension;

namespace GadgetExchange.Respository.Interface.Controllers.Async
{
    public interface IGadgetRepositoryAsync : IDisposable
    {
        Task<IEnumerable<Gadget>> GetGadgets();

        Task<IEnumerable<Gadget>> FilterGadgetByStatus(Status status);

        Task<Gadget> GetGadgetById(int id);

        Task SetRentalPrice(int id, float price);

        Task Save();
    }
}
