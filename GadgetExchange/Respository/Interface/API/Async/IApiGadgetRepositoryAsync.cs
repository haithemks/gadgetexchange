﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GadgetExchange.Models.Extension;

namespace GadgetExchange.Respository.Interface.API.Async
{
    public interface IApiGadgetRepositoryAsync
    {
        #region Gadget

        Task<IEnumerable<Gadget>> GetAllGadgets();
        Task<Gadget> GetGadgetById(int id);
        Task AddGadget(Gadget item);
        Task UpdateGadget(Gadget item);
        Task RemoveGadget(int id);
        Task<bool> GadgetExist(int id);

        #endregion

    }
}
