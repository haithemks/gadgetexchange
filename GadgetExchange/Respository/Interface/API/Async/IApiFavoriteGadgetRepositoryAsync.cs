﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GadgetExchange.Models.Extension;

namespace GadgetExchange.Respository.Interface.API.Async
{
    public interface IApiFavoriteGadgetRepositoryAsync
    {
        #region FavoriteGadget

        Task<IEnumerable<FavoriteGadget>> GetAllFavoriteGadgets();
        Task<FavoriteGadget> GetFavoriteGadgetById(int id);
        Task AddFavoriteGadget(int gadgetId, string id);
        Task<bool> RemoveFavoriteGadget(int gadgetId, string id);
        Task<bool> FavoriteGadgetExist(int id);
        Task<bool> FavoriteGadgetExist(int gadgetId, string userId);

        #endregion
    }
}
