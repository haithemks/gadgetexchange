﻿using System.Collections.Generic;
using GadgetExchange.Models.Extension;

namespace GadgetExchange.Respository.Interface.API
{
    public interface IApiGadgetRepository
    {
        #region Gadget

        IEnumerable<Gadget> GetAllGadgets();
        Gadget GetGadgetById(int id);
        void AddGadget(Gadget item);
        void UpdateGadget(Gadget item);
        void RemoveGadget(int id);
        bool GadgetExist(int id);

        #endregion

    }
}
