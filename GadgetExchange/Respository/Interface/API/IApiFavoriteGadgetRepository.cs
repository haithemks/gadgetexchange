﻿using System.Collections.Generic;
using GadgetExchange.Models.Extension;

namespace GadgetExchange.Respository.Interface.API
{
    public interface IApiFavoriteGadgetRepository
    {
        #region FavoriteGadget

        IEnumerable<FavoriteGadget> GetAllFavoriteGadgets();
        FavoriteGadget GetFavoriteGadgetById(int id);
        void AddFavoriteGadget(int gadgetId, string id);
        bool RemoveFavoriteGadget(int gadgetId, string id);
        bool FavoriteGadgetExist(int id);
        bool FavoriteGadgetExist(int gadgetId, string userId);

        #endregion
    }
}
