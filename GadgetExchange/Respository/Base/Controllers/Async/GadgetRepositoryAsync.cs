﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.Controllers.Async;

namespace GadgetExchange.Respository.Base.Controllers.Async
{
    public class GadgetRepositoryAsync : IGadgetRepositoryAsync
    {
        private readonly ApplicationDbContext _context;

        private bool _disposed;

        #region Gadget

        public GadgetRepositoryAsync()
        {
            _context = new ApplicationDbContext();
        }

        public GadgetRepositoryAsync(ApplicationDbContext dbContext)
        {
            _context = dbContext;
        }

        public async Task<IEnumerable<Gadget>> GetGadgets()
        {
            return await _context.Gadgets.ToListAsync();
        }

        public async Task<IEnumerable<Gadget>> FilterGadgetByStatus(Status status)
        {
            return await _context.Gadgets.Where(x => x.StatusId == status).ToListAsync();
        }

        public async Task<Gadget> GetGadgetById(int id)
        {
            return await _context.Gadgets.FindAsync(id);
        }

        public async Task SetRentalPrice(int id, float price)
        {
            var gadget = await GetGadgetById(id);
            gadget.Cost = price;
            await Save();
        }
        
        #endregion

        #region Common

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        #endregion

    }
}