﻿using System;
using System.Collections.Generic;
using System.Linq;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.Controllers;

namespace GadgetExchange.Respository.Base.Controllers
{
    public class GadgetRepository : IGadgetRepository
    {
        private readonly ApplicationDbContext _context;

        private bool _disposed;

        #region Gadget

        public GadgetRepository()
        {
            _context = new ApplicationDbContext();
        }

        public GadgetRepository(ApplicationDbContext dbContext)
        {
            _context = dbContext;
        }

        public IEnumerable<Gadget> GetGadgets()
        {
            return _context.Gadgets.ToList();
        }

        public IEnumerable<Gadget> FilterGadgetByStatus(Status status)
        {
            return _context.Gadgets.Where(x => x.StatusId == status).ToList();
        }

        public Gadget GetGadgetById(int id)
        {
            return _context.Gadgets.Find(id);
        }

        public void SetRentalPrice(int id, float price)
        {
            var gadget = GetGadgetById(id);
            gadget.Cost = price;
            Save();
        }
        
        #endregion

        #region Common

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        #endregion

    }
}