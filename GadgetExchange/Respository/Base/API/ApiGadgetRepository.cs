﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API;

namespace GadgetExchange.Respository.Base.API
{
    public class ApiGadgetRepository : IApiGadgetRepository, IDisposable
    {
        private readonly ApplicationDbContext _context;

        private bool _disposed;

        public ApiGadgetRepository()
        {
            _context = new ApplicationDbContext();
        }
        public ApiGadgetRepository(ApplicationDbContext dbContext)
        {
            _context = dbContext;
        }

        #region Gadget

        public IEnumerable<Gadget> GetAllGadgets()
        {
            return _context.Gadgets.ToList();
        }

        public Gadget GetGadgetById(int id)
        {
            return GadgetExist(id) ? _context.Gadgets.Find(id) : null;
        }

        public void AddGadget(Gadget item)
        {
            _context.Gadgets.Add(item);
            Save();
        }

        public void UpdateGadget(Gadget item)
        {
            _context.Gadgets.AddOrUpdate(item);
            Save();
        }

        public void RemoveGadget(int id)
        {
            _context.Gadgets.Remove(GetGadgetById(id));
            Save();
        }

        public bool GadgetExist(int id)
        {
            return _context.Gadgets.Count(e => e.Id == id) > 0;
        }

        #endregion

        #region Common

        private void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
        
        #endregion
    }
}