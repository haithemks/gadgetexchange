﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API;

namespace GadgetExchange.Respository.Base.API
{
    public class ApiFavoriteGadgetRepository : IApiFavoriteGadgetRepository, IDisposable
    {
        private readonly ApplicationDbContext _context;

        private bool _disposed;

        public ApiFavoriteGadgetRepository()
        {
            _context = new ApplicationDbContext();
        }

        public ApiFavoriteGadgetRepository(ApplicationDbContext dbContext)
        {
            _context = dbContext;
        }

        #region FavoriteGadget

        public IEnumerable<FavoriteGadget> GetAllFavoriteGadgets()
        {
            return _context.FavoriteGadgets.ToList();
        }

        public FavoriteGadget GetFavoriteGadgetById(int id)
        {
            return FavoriteGadgetExist(id) ? _context.FavoriteGadgets.Find(id) : null;
        }

        public void AddFavoriteGadget(int gadgetId, string userId)
        {
            var user = _context.Users.First(m => m.Id == userId);
            if (!FavoriteGadgetExist(gadgetId, userId))
            {
                var gadget = _context.Gadgets.Find(gadgetId);
                if (gadget != null)
                {
                    gadget.StatusId = Status.Favorite;
                    var newFavoriteGadget = new FavoriteGadget
                    {
                        User = user,
                        GadgetId = gadgetId
                    };
                    _context.FavoriteGadgets.Add(newFavoriteGadget);
                   // _context.Entry(newFavoriteGadget).State = EntityState.Added;
                    Save();
                }
            }
        }

        public bool RemoveFavoriteGadget(int gadgetId, string userId)
        {
            if (FavoriteGadgetExist(gadgetId, userId))
            {
                var gadget = _context.Gadgets.Find(gadgetId);
                var favoriteGadget = _context.FavoriteGadgets.FirstOrDefault(e => e.GadgetId == gadgetId && e.User.Id == userId);

                if (gadget != null && favoriteGadget!= null)
                {
                    gadget.StatusId = Status.Available;
                    _context.FavoriteGadgets.Remove(favoriteGadget);
                    Save();
                    return true;
                }
                return false;
            }
            return false;
        }

        public bool FavoriteGadgetExist(int id)
        {
            return _context.FavoriteGadgets.Count(e => e.Id == id) > 0;
        }

        public bool FavoriteGadgetExist(int gadgetId, string userId)
        {
            return _context.FavoriteGadgets.Count(e => e.GadgetId == gadgetId && e.User.Id == userId) > 0;
        }

        #endregion

        #region Common

        private void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        #endregion
    }
}