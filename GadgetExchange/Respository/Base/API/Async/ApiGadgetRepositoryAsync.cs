﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API.Async;

namespace GadgetExchange.Respository.Base.API.Async
{
    public class ApiGadgetRepositoryAsync : IApiGadgetRepositoryAsync, IDisposable
    {
        private readonly ApplicationDbContext _context;

        private bool _disposed;

        public ApiGadgetRepositoryAsync()
        {
            _context = new ApplicationDbContext();
        }

        public ApiGadgetRepositoryAsync(ApplicationDbContext dbContext)
        {
            _context = dbContext;
        }

        #region Gadget

        public async Task<IEnumerable<Gadget>> GetAllGadgets()
        {
            return await _context.Gadgets.ToListAsync();
        }

        public async Task<Gadget> GetGadgetById(int id)
        {
            return await GadgetExist(id) ? await _context.Gadgets.FindAsync(id) : null;
        }

        public async Task AddGadget(Gadget item)
        {
            _context.Gadgets.Add(item);
            await Save();
        }

        public async Task UpdateGadget(Gadget item)
        {
            _context.Gadgets.AddOrUpdate(item);
            await Save();
        }

        public async Task RemoveGadget(int id)
        {
            _context.Gadgets.Remove(await GetGadgetById(id));
            await Save();
        }

        public async Task<bool> GadgetExist(int id)
        {
            return await _context.Gadgets.CountAsync(e => e.Id == id) > 0;
        }

        #endregion

        #region Common

        private async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
        
        #endregion
    }
}