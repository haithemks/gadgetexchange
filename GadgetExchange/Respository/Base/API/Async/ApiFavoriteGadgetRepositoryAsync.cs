﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API.Async;

namespace GadgetExchange.Respository.Base.API.Async
{
    public class ApiFavoriteGadgetRepositoryAsync : IApiFavoriteGadgetRepositoryAsync, IDisposable
    {
        private readonly ApplicationDbContext _context;

        private bool _disposed;

        public ApiFavoriteGadgetRepositoryAsync()
        {
            _context = new ApplicationDbContext();
        }

        public ApiFavoriteGadgetRepositoryAsync(ApplicationDbContext dbContext)
        {
            _context = dbContext;
        }

        #region FavoriteGadget

        public async Task<IEnumerable<FavoriteGadget>> GetAllFavoriteGadgets()
        {
            return await _context.FavoriteGadgets.ToListAsync();
        }

        public async Task<FavoriteGadget> GetFavoriteGadgetById(int id)
        {
            return await FavoriteGadgetExist(id) ? await _context.FavoriteGadgets.FindAsync(id) : null;
        }

        public async Task AddFavoriteGadget(int gadgetId, string userId)
        {
            var user = await _context.Users.FirstAsync(m => m.Id == userId);
            if (! await FavoriteGadgetExist(gadgetId, userId))
            {
                var gadget = await _context.Gadgets.FindAsync(gadgetId);
                if (gadget != null)
                {
                    gadget.StatusId = Status.Favorite;
                    var newFavoriteGadget = new FavoriteGadget
                    {
                        User = user,
                        GadgetId = gadgetId
                    };
                    _context.FavoriteGadgets.Add(newFavoriteGadget);
                    await Save();
                }
            }
        }

        public async Task<bool> RemoveFavoriteGadget(int gadgetId, string userId)
        {
            if (await FavoriteGadgetExist(gadgetId, userId))
            {
                var gadget = await _context.Gadgets.FindAsync(gadgetId);
                var favoriteGadget = await _context.FavoriteGadgets.FirstOrDefaultAsync(e => e.GadgetId == gadgetId && e.User.Id == userId);

                if (gadget != null && favoriteGadget!= null)
                {
                    gadget.StatusId = Status.Available;
                    _context.FavoriteGadgets.Remove(favoriteGadget);
                    await Save();
                    return true;
                }
                return false;
            }
            return false;
        }

        public async Task<bool> FavoriteGadgetExist(int id)
        {
            return await _context.FavoriteGadgets.CountAsync(e => e.Id == id) > 0;
        }

        public async Task<bool> FavoriteGadgetExist(int gadgetId, string userId)
        {
            return await _context.FavoriteGadgets.CountAsync(e => e.GadgetId == gadgetId && e.User.Id == userId) > 0;
        }

        #endregion

        #region Common

        private async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        #endregion
    }
}