﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GadgetExchange.Startup))]
namespace GadgetExchange
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
