﻿
$(document).ready(function () {

    $('ul.dropdown-menu li a').click(function (e) {
        var url = "/gadgetexchange/home?filter=" + $(this).attr("data-item");
        window.location.href = url;        
    });

    function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

    var foo = getUrlParameter('filter');

    if (foo !== 'undefined') {
        $('ul.dropdown-menu li a').each(function () {
            var value = $(this).attr("data-item");
            var item = $(this).text();
            var caret = item + ("  <span class='caret'></span>");
            if (value == foo) {
                $('#featureButton').html(caret);
            }
        });
    }

    $(".queue").click(function () {
        swal("Congratulation!", "Your Gadget is queued.", "success");
    });

    $(".exchange").click(function () {
        swal({
            title: "Sweet!",
            text: "You will be notified when Gadgeter accept your Offer",
            imageUrl: '/gadgetexchange/assets/thumbs-up.jpg'
        });
    });

    $(".setPrice").click(function () {
        swal({
            title: "Make Some Money!",
            text: "How much you want to rent your gadget for 1 week In Us Dollars?",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            inputPlaceholder: "25.00"
        }, function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to Set your Prices!");
                return false
            }
            swal("Great!", "Your Price: " + inputValue + " is set.", "success");
        });
    });

    $(".rent").click(function () {
        var priceItem = $(this).attr("data-item") + " $";
        swal({
            title: "Rent Gadget Request",
            text: priceItem,
            type: "success",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
            setTimeout(function () {
                swal("Rent Request Transmitted. You will get a confirmation Email soon!");
            }, 2000);
        });
    });

    $(document).on('click', ".fa-heart-not", function () {
        var gadgetId = $(this).attr("data-item");
        var current = $(this);
        var url = "/gadgetexchange/Account/Login?ReturnUrl=home";
        $.post("/gadgetexchange/api/FavoriteGadget?id=" + gadgetId)
            .done(function (data) {
                if (data.Message === 'Authorization has been denied for this request.') {
                    window.location.href = url;
                } else {
                swal("Super!", "Gadget added to Favorite.", "success");
                current.addClass('fa-heart').removeClass('fa-heart-not');
                }
            })
            .fail(function () {
                window.location.href = url;
            });
    });

    $(document).on('click',".fa-heart", function () {
        var gadgetId = $(this).attr("data-item");
        var current = $(this);
        var url = "/gadgetexchange/Account/Login?ReturnUrl=home";
        $.ajax({
            url: '/gadgetexchange/api/FavoriteGadget/delete?id=' + gadgetId,
            type: 'DELETE',
            success: function (data) {
                if (data.Message === 'Authorization has been denied for this request.') {
                    window.location.href = url;
                } else {
                current.addClass('fa-heart-not').removeClass('fa-heart');
                swal("Duper!", "Gadget removed from Favorite.", "success");
                }
            },
            error: function (data) {
                console.log(data.Message);
               // window.location.href = url;
            }
        });
    });
});