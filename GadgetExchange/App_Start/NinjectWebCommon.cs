using System;
using System.Web;
using GadgetExchange;
using GadgetExchange.Respository.Base;
using GadgetExchange.Respository.Base.API;
using GadgetExchange.Respository.Base.API.Async;
using GadgetExchange.Respository.Base.Controllers;
using GadgetExchange.Respository.Base.Controllers.Async;
using GadgetExchange.Respository.Interface;
using GadgetExchange.Respository.Interface.API;
using GadgetExchange.Respository.Interface.API.Async;
using GadgetExchange.Respository.Interface.Controllers;
using GadgetExchange.Respository.Interface.Controllers.Async;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace GadgetExchange
{
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            Bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {

            kernel.Bind<IGadgetRepository>().To<GadgetRepository>().InRequestScope();
            kernel.Bind<IApiGadgetRepository>().To<ApiGadgetRepository>().InRequestScope();
            kernel.Bind<IApiFavoriteGadgetRepository>().To<ApiFavoriteGadgetRepository>().InRequestScope();

            kernel.Bind<IGadgetRepositoryAsync>().To<GadgetRepositoryAsync>().InRequestScope();
            kernel.Bind<IApiGadgetRepositoryAsync>().To<ApiGadgetRepositoryAsync>().InRequestScope();
            kernel.Bind<IApiFavoriteGadgetRepositoryAsync>().To<ApiFavoriteGadgetRepositoryAsync>().InRequestScope();

        }
    }
}
