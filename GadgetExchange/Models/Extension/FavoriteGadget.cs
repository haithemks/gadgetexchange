﻿using System.ComponentModel.DataAnnotations;

namespace GadgetExchange.Models.Extension
{
//FavoriteId
//GadgetId
    public class FavoriteGadget
    {
        public int Id { get; set; }
        [Required]
        public ApplicationUser User { get; set; }

        [Required]
        public int GadgetId { get; set; }
    }
}