﻿using System.ComponentModel.DataAnnotations;

namespace GadgetExchange.Models.Extension
{
    public class Gadget
    { 
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        public string ImagePath { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public int ReviewsCount { get; set; }

        [Required]
        public Status StatusId { get; set; }

        [DisplayFormat(DataFormatString = "{0:C0}")]

        public double? Cost { get; set; }

    }


    public enum Status
    {
        All = 0,
        Available = 1,
        Queued = 2,
        Mine = 3,
        Favorite = 4
    }


    public class Statu
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }

}
