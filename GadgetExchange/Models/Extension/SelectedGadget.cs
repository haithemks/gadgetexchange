﻿using System.ComponentModel.DataAnnotations;

namespace GadgetExchange.Models.Extension
{

//MineId
//GadgetId
    public class SelectedGadget
    {
        public int Id { get; set; }

        [Required]

        public ApplicationUser User { get; set; }

        [Required]
        public int GadgetId { get; set; }
    }
}