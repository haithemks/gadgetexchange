﻿using System.Data.Common;
using System.Data.Entity;
using GadgetExchange.Models.Extension;
using Microsoft.AspNet.Identity.EntityFramework;

namespace GadgetExchange.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Gadget> Gadgets { get; set; }
        public DbSet<SelectedGadget> SelectedGadgets { get; set; }
        public DbSet<FavoriteGadget> FavoriteGadgets { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public ApplicationDbContext(DbConnection connection)
            : base(connection, true)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}