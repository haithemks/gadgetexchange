namespace GadgetExchange.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class CreateGadgetTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FavoriteGadgets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GadgetId = c.Int(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Gadgets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ImagePath = c.String(),
                        Description = c.String(),
                        ReviewsCount = c.Int(nullable: false),
                        StatusId = c.Int(nullable: false),
                        User_Id = c.String(maxLength: 128),
                        Cost = c.Double()
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.SelectedGadgets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GadgetId = c.Int(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SelectedGadgets", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Gadgets", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.FavoriteGadgets", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.SelectedGadgets", new[] { "User_Id" });
            DropIndex("dbo.Gadgets", new[] { "User_Id" });
            DropIndex("dbo.FavoriteGadgets", new[] { "User_Id" });
            DropTable("dbo.SelectedGadgets");
            DropTable("dbo.Gadgets");
            DropTable("dbo.FavoriteGadgets");
        }
    }
}
