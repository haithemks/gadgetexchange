namespace GadgetExchange.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class PopulateGadgetTable : DbMigration
    {
        public override void Up()
        {
            Sql(" SET IDENTITY_INSERT Gadgets ON " +
                "INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost ) VALUES" +
                "(1,'Apple Watch Series 1','../../assets/images/devices/APPLE_WATCH_SERIES_1.jpeg','Durable, lightweight aluminum cases. Sport Band in a variety of colors. All the features of the Apple Watch, plus a new dual-core processor for faster performance. All models run watchOS 3.',88,1 , 25.50)");

            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(2,'Apple Watch Series 3','../../assets/images/devices/APPLE_WATCH_SERIES_3.png','Answer a call from your surfboard. Ask Siri to send a message. Stream your favorite songs on your run.� And do it all while leaving your phone behind. Introducing Apple Watch Series 3.',129,1 , 27.50)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(3,'Asus Zenpad Tablet','../../assets/images/devices/asus-zenpad table.jpeg','Take your favorite books and apps with you wherever you roam with this Asus ZenPad tablet, featuring Android 6.0 Marshmallow for reliable program support with IPS technology.',58,4, 29.99)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(4,'Asus Zenfone 3 Deluxe','../../assets/images/devices/asus_zenfone_deluxe.jpg','Enjoy supercharged performance around the clock with this Asus Zenfone 3 Deluxe smartphone. The 6GB of RAM and Qualcomm Snapdragon 820 processor provide blazing functionality.',102,4, 30.15)");

            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(5,'Fitbit Ionic','../../assets/images/devices/Fitbit_Ionic.jpg','Maximize workouts with this Fitbit Ionic fitness watch. It provides dynamic personal coaching, heart rate monitoring and storage for over 300 songs to help you stay motivated.',96,1, 16.00)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(6,'Galaxy S III I747','../../assets/images/devices/GalaxyS3.jpg','With a Super AMOLED capacitive touch screen and built-in Wi-Fi, this Samsung Galaxy S III I747 WHITE cell phone makes it easy to navigate Web content while you are on the go.',105,3 , 17.50)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(7,'Samsung Galaxy S4','../../assets/images/devices/GalaxyS4.jpg','Quickly communicate with friends and family using this Samsung Galaxy S4 smartphone. The 5-inch screen makes it easy to communicate with people crystal clear.',92,1 , 24.99)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(8,'Samsung Galaxy S6','../../assets/images/devices/GalaxyS6.jpg','This phone delivers fast processing power and an HD display in a sleek, stylish design. A 16.0MP rear camera lets you capture vivid photos and stunning videos to preserve any moment.',23,3, 28.99)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(9,'Samsung Galaxy S7 Edge','../../assets/images/devices/GalaxyS7.jpg','Work, rest and play with the Samsung Galaxy S7 edge. A massive 5.5-inch Super AMOLED touchscreen goes all the way to the edges of the phone while you are on the go.',68,1, 15.50)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(10,'HTC One','../../assets/images/devices/htc_one.jpg','Flaunts a stunning metal design, a powerful quad-core processor, and a beautiful 4.7-inch, 1080p screen. It runs Android Jelly Bean, takes great pictures, and has a feature-packed camera app.',54,1, 19.99)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(11,'IPad','../../assets/images/devices/Ipad.jpg','With a gorgeous 9.7-inch Retina display and a design that�s only 6.1mm thin and weighs just 0.96 pounds, iPad Air 2 is as portable as it is capable and up to 10 hours of battery life.',38,1 , 29.99)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(12,'IPad Mini','../../assets/images/devices/IpadMini.jpg','Powerful A8 chip with 64-bit desktop-class architecture, iSight and FaceTime HD cameras, Touch ID fingerprint sensor, Wi-Fi and LTE connectivity,� and up to 10 hours of battery life.',75,2 , 39.99)");

            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(13,'IPad Pro','../../assets/images/devices/IpadPro.jpg','With its epic Retina display, the 12.9-inch iPad Pro is the largest and most capable iPad ever. It has a powerful A9X chip with 64-bit desktop-class architecture and advanced iSight.',64,1, 49.99)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(14,'iPhone 6','../../assets/images/devices/Iphone6.jpg','Keep your contacts and important documents close at hand with this Apple iPhone 6, which connects with iCloud to share documents and information with your computer and 3D Touch.',49,1 , 59.99)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(15,'iPhone 7','../../assets/images/devices/Iphone7.jpg','iPhone 7 features an all-new 12MP camera with an �/1.8 aperture for great low-light photos and 4K video. Optical image stabilization. A 4.7-inch Retina HD display with wide color and 3D Touch.',99,1 , 17.99)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(16,'Keorker Smart Watch','../../assets/images/devices/KEOKER_KW18 SMART_WATCH.jpg','Bluetooth Smart Watch, Keoker 1.3 inches IPS Round Touch Screen Water Resistant Smartwatch Phone with SIM Card Slot,Sleep Monitor,Heart Rate Monitor and Pedometer for Devices.',135,2 ,30)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(17,'Galaxy Tab S3','../../assets/images/devices/my-galaxy-tab-s3.jpg','Work, play and communicate with this Samsung Galaxy Tab S3 featuring a 2.15 GHz + 1.6 GHz quad core processor. It has an ultra-clear Super AMOLED display, so you can watch movies.',105,1 , 25.50)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(18,'Samsung Galaxy S8','../../assets/images/devices/S8.jpg','Work and play with this Samsung Galaxy S8 smartphone. It has a 5.8-inch Quad HD+ infinity display for excellent image quality, and its 64GB of ROM handles multiple applications without interruption.',78,1 ,15.50)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(19,'Samsung Galaxy S9','../../assets/images/devices/S9Edge.jpg','S9 Unlocked 3G/2G Smartphone 6 inch HD Screen(1280x720) Android 5.1 Mobile Phone-MTK6580 Quad Core 1GB RAM 8GB ROM 1.3GHz Dual Sim Dual Camera WIFI GPS G-Sensor Touchscreen SIM.',75,1 ,49.99)");


            Sql("INSERT INTO Gadgets " +
                "(Id, Name, ImagePath, [Description], ReviewsCount, StatusId, Cost) VALUES" +
                "(20,'Gear S3 frontier','../../assets/images/devices/SAMSUNG_GEAR S3_FRONTIER.jpg','With Under Armour Connected Fitness on your Samsung  you now have more fitness tracking options at your fingertips. You can download the MapMyRun, MyFitnessPal or any Edumondo  apps in the market.',101,2, 39.99)" +
                "SET IDENTITY_INSERT Gadgets OFF");


        }


        public override void Down()
        {
        }
    }
}
