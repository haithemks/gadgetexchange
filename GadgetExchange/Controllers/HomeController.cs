﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GadgetExchange.Helpers;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface;
using GadgetExchange.Respository.Interface.Controllers;
using PagedList;

namespace GadgetExchange.Controllers
{
    public class HomeController : Controller
    {
        private readonly IGadgetRepository _repo;

        public HomeController(IGadgetRepository repo)
        {
            _repo = repo;
        }

        public ActionResult Index(int? page, string filter = null)
        {
            var filters = String.IsNullOrEmpty(filter) ? "0" : filter;
            List<Gadget> gadgets;
            switch (EnumHelper.Parse<Status>(filters))
            {
                case Status.Available:
                    gadgets = _repo.FilterGadgetByStatus(Status.Available).ToList();
                    break;
                case Status.Queued:
                    gadgets = _repo.FilterGadgetByStatus(Status.Queued).ToList();
                    break;
                case Status.Mine:
                    gadgets = _repo.FilterGadgetByStatus(Status.Mine).ToList();
                    break;
                case Status.Favorite:
                    gadgets = _repo.FilterGadgetByStatus(Status.Favorite).ToList();
                    break;
                default:
                    gadgets = _repo.GetGadgets().ToList();
                    break;
            }
            var pageSize = 9;
            var pageNumber = (page ?? 1);
            return View(gadgets.ToPagedList(pageNumber, pageSize));
        }

        [ChildActionOnly]
        public ActionResult StatusSelection()
        {
            var enumHelper = new EnumHelper();
            var allStatus = (from Status n in Enum.GetValues(typeof(Status))
                                select new
                                    Statu
                                    {
                                        Id = (int)n,
                                        Description = enumHelper.GetEnumDescription(n)
                                     }
                             ).ToList();
            return PartialView(allStatus);
        }
    }
}