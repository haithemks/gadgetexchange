﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using GadgetExchange.Helpers;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.Controllers.Async;
using PagedList;

namespace GadgetExchange.Controllers
{
    public class GadgetsController : Controller
    {
        private readonly IGadgetRepositoryAsync _repo;

        public GadgetsController(IGadgetRepositoryAsync repo)
        {
            _repo = repo;
        }

        public async Task<ActionResult> Index(int? page, string filter = null)
        {
            var filters = String.IsNullOrEmpty(filter) ? "0" : filter;
            Task<IEnumerable<Gadget>> gadgets;
            switch (EnumHelper.Parse<Status>(filters))
            {
                case Status.Available:
                    gadgets = _repo.FilterGadgetByStatus(Status.Available);
                    break;
                case Status.Queued:
                    gadgets = _repo.FilterGadgetByStatus(Status.Queued);
                    break;
                case Status.Mine:
                    gadgets = _repo.FilterGadgetByStatus(Status.Mine);
                    break;
                case Status.Favorite:
                    gadgets = _repo.FilterGadgetByStatus(Status.Favorite);
                    break;
                default:
                    gadgets = _repo.GetGadgets();
                    break;
            }
            var pageSize = 9;
            var pageNumber = (page ?? 1);
            var result = await Task.Run(() => gadgets.Result.ToPagedList(pageNumber, pageSize));
            return View(result);
        }

        [ChildActionOnly]
        public ActionResult StatusSelection()
        {
            var enumHelper = new EnumHelper();
            var allStatus = (from Status n in Enum.GetValues(typeof(Status))
                select new
                    Statu
                    {
                        Id = (int)n,
                        Description = enumHelper.GetEnumDescription(n)
                    }
            ).ToList();
            return PartialView(allStatus);
        }
    }
}
