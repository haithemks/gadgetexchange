﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData;
using GadgetExchange.Models;
using GadgetExchange.Models.Extension;

namespace GadgetExchange.API
{
    /*
     * Endpoint
     * https://localhost:44382/odata/$metadata#GadgetOData
     * https://localhost:44382/odata/GadgetOData/?GetGadgetOData
     * https://localhost:44382/odata/GadgetOData/?$top=1
     * https://localhost:44382/odata/GadgetOData/?$select=Id
     * https://localhost:44382/odata/GadgetOData(5)
     * https://localhost:44382/odata/GadgetOData/?$orderby=Id desc
     * https://localhost:44382/odata/GadgetOData/?$filter=Id eq 1
     * https://localhost:44382/odata/GadgetOData/?$inlinecount=allpages
     * Endpoint

    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using GadgetExchange.Models.Extension;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Gadget>("GadgetOData");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class GadgetODataController : ODataController
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        // GET: odata/GadgetOData
        [EnableQuery]
        public IQueryable<Gadget> GetGadgetOData()
        {
            return _db.Gadgets.AsQueryable();
        }

        // GET: odata/GadgetOData(5)
        [EnableQuery]
        public SingleResult<Gadget> GetGadget([FromODataUri] int key)
        {
            return SingleResult.Create(_db.Gadgets.Where(gadget => gadget.Id == key));
        }

        // PUT: odata/GadgetOData(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<Gadget> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Gadget gadget = await _db.Gadgets.FindAsync(key);
            if (gadget == null)
            {
                return NotFound();
            }

            patch.Put(gadget);

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GadgetExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(gadget);
        }

        // POST: odata/GadgetOData
        public async Task<IHttpActionResult> Post(Gadget gadget)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.Gadgets.Add(gadget);
            await _db.SaveChangesAsync();

            return Created(gadget);
        }

        // PATCH: odata/GadgetOData(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<Gadget> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Gadget gadget = await _db.Gadgets.FindAsync(key);
            if (gadget == null)
            {
                return NotFound();
            }

            patch.Patch(gadget);

            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GadgetExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(gadget);
        }

        // DELETE: odata/GadgetOData(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            Gadget gadget = await _db.Gadgets.FindAsync(key);
            if (gadget == null)
            {
                return NotFound();
            }

            _db.Gadgets.Remove(gadget);
            await _db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GadgetExists(int key)
        {
            return _db.Gadgets.Count(e => e.Id == key) > 0;
        }
    }
}
