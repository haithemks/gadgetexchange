﻿using System;
using System.Linq;
using System.Web.Http;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API;
using Microsoft.AspNet.Identity;

namespace GadgetExchange.API
{
    [Authorize]
    public class FavoriteGadgetController : ApiController
    {
        public IApiFavoriteGadgetRepository Repo;

        public FavoriteGadgetController(IApiFavoriteGadgetRepository repo)
        {
            Repo = repo;
        }

        #region FavoriteGadget

        public IQueryable<FavoriteGadget> GetFavoriteGadgets()
        {
            return Repo.GetAllFavoriteGadgets().AsQueryable();
        }

        [HttpGet]
        public IHttpActionResult GetFavoriteGadget(int id)
        {
            try
            {
                var favoritegadget = Repo.GetFavoriteGadgetById(id);
                if (favoritegadget == null)
                {
                    return NotFound();
                }
                return Ok(favoritegadget);
            }
            catch (Exception)
            {
                return BadRequest($"Favorite Gadget not Found:{id}");
            }
        }

        [HttpPost]
        public IHttpActionResult PostFavoriteGadget(int id, string userId = null)
        {
            try
            {
                if (userId == null)
                {
                    userId = User.Identity.GetUserId();
                }
                Repo.AddFavoriteGadget(id, userId);
                var newUri = Url.Link("DefaultApi", new { id });
                return Created(newUri, id);
            }
            catch (Exception)
            {
                return BadRequest($"Favorite Gadgets:{id} can't be added");
            }
        }

        [HttpDelete]
        public IHttpActionResult DeleteFavoriteGadget(int id, string userId = null)
        {
            try
            {
                if (userId == null)
                {
                    userId = User.Identity.GetUserId();
                }
                var result = Repo.RemoveFavoriteGadget(id, userId);
                if (result)
                {
                    return Ok(id);
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest($"Favorite Gadgets:{id} can't be removed");
            }
        }

        #endregion
    }
}