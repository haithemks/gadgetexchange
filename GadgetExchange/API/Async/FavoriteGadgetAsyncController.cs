﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API.Async;
using Microsoft.AspNet.Identity;

namespace GadgetExchange.API.Async
{
    [Authorize]
    public class FavoriteGadgetAsyncController : ApiController
    {
        private readonly IApiFavoriteGadgetRepositoryAsync _repo;

        public FavoriteGadgetAsyncController(IApiFavoriteGadgetRepositoryAsync repo)
        {
            _repo = repo;
        }

        #region FavoriteGadget

        public async Task<IQueryable<FavoriteGadget>> GetFavoriteGadgets()
        {
            var favoriteGadgetList = await _repo.GetAllFavoriteGadgets();
            return favoriteGadgetList.AsQueryable();
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetFavoriteGadget(int id)
        {
            try
            {
                var favoritegadget = await _repo.GetFavoriteGadgetById(id);
                if (favoritegadget == null)
                {
                    return NotFound();
                }
                return Ok(favoritegadget);
            }
            catch (Exception)
            {
                return BadRequest($"Favorite Gadget not Found:{id}");
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> PostFavoriteGadget(int id, string userId = null)
        {
            try
            {
                if (userId == null)
                {
                    userId = User.Identity.GetUserId();
                }
                await _repo.AddFavoriteGadget(id, userId);
                var newUri = Url.Link("DefaultApi", new { id });
                return Created(newUri, id);
            }
            catch (Exception)
            {
                return BadRequest($"Favorite Gadgets:{id} can't be added");
            }
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteFavoriteGadget(int id, string userId = null)
        {
            try
            {
                if (userId == null)
                {
                    userId = User.Identity.GetUserId();
                }
                var result = await _repo.RemoveFavoriteGadget(id, userId);
                if (result)
                {
                    return Ok(id);
                }
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest($"Favorite Gadgets:{id} can't be removed");
            }
        }

        #endregion
    }
}