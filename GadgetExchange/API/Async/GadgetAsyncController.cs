﻿using System;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API.Async;

namespace GadgetExchange.API.Async
{
    public class GadgetAsyncController : ApiController
    {
        private readonly IApiGadgetRepositoryAsync _repo;

        public GadgetAsyncController(IApiGadgetRepositoryAsync repo)
        {
            _repo = repo;
        }

        #region Gadget
        public async Task<IQueryable<Gadget>> GetGadgets()
        {
            var gadgetList = await _repo.GetAllGadgets();
            return gadgetList.AsQueryable();
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetGadget(int id)
        {
            try
            {
                var gadget = await _repo.GetGadgetById(id);
                if (gadget == null)
                {
                    return NotFound();
                }
                return Ok(gadget);
            }
            catch (Exception)
            {
                return BadRequest($"Gadget not Found:{id}");
            }
        }

        [HttpPut]
        public async Task<IHttpActionResult> PutGadget(int id, Gadget gadget)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != gadget.Id)
            {
                return BadRequest();
            }
            try
            {
                await Task.Run(() =>
                {
                    _repo.UpdateGadget(gadget);
                });
            }

            catch (Exception)
            {
                return BadRequest($"Gadget not Updated:{id}");
            }
            return Content(HttpStatusCode.Accepted, gadget);
        }

        [HttpPost]
        [ActionName("AddGadget")]
        public async Task<IHttpActionResult> PostGadget([FromBody]Gadget gadget)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await Task.Run(() =>
            {
                _repo.AddGadget(gadget);
            });
            var newUri = Url.Link("DefaultApi", new {id = gadget.Id});
            return Created(newUri, gadget);
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteGadget(int id)
        {
            var result  = await Task.Run(() => _repo.GetGadgetById(id));
            Gadget gadget = result;
            if (gadget == null)
            {
                return NotFound();
            }

            await Task.Run(() =>
            {
                 _repo.RemoveGadget(id);   
             });

            return Ok(gadget);         
        }

        #endregion

    }
}