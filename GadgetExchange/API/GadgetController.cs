﻿using System;
using System.Linq;
using System.Net;
using System.Web.Http;
using GadgetExchange.Models.Extension;
using GadgetExchange.Respository.Interface.API;

namespace GadgetExchange.API
{
    public class GadgetController : ApiController
    {
        private readonly IApiGadgetRepository _repo;

        public GadgetController(IApiGadgetRepository repo)
        {
            _repo = repo;
        }

        #region Gadget
        public IQueryable<Gadget> GetGadgets()
        {
            return _repo.GetAllGadgets().AsQueryable();
        }

        [HttpGet]
        public IHttpActionResult GetGadget(int id)
        {
            try
            {
                var gadget = _repo.GetGadgetById(id);
                if (gadget == null)
                {
                    return NotFound();
                }
                return Ok(gadget);
            }
            catch (Exception)
            {
                return BadRequest($"Gadget not Found:{id}");
            }
        }

        [HttpPut]
        public IHttpActionResult PutGadget(int id, Gadget gadget)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != gadget.Id)
            {
                return BadRequest();
            }
            try
            {
                _repo.UpdateGadget(gadget);
            }

            catch (Exception)
            {
                return BadRequest($"Gadget not Updated:{id}");
            }
            return Content(HttpStatusCode.Accepted, gadget);
        }

        [HttpPost]
        [ActionName("AddGadget")]
        public IHttpActionResult PostGadget([FromBody]Gadget gadget)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repo.AddGadget(gadget);
            var newUri = Url.Link("DefaultApi", new {id = gadget.Id});
            return Created(newUri, gadget);
        }

        [HttpDelete]
        public IHttpActionResult DeleteGadget(int id)
        {
            Gadget gadget = _repo.GetGadgetById(id);
            if (gadget == null)
            {
                return NotFound();
            }
            _repo.RemoveGadget(id);
            return Ok(gadget);
        }

        #endregion

    }
}