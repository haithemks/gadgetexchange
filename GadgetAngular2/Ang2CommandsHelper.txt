np install
ng serve
localhost:4200
ng test
ng e2e
ng build
ng build --prod
ng new Test
ng g c products/product-detail.component --flat
ng g c products/product-detail.component --flat -m app.module
ng help
ng help -v
ng v --help
ng v
ng new hello --prefix h
ng generate
ng g
	cl	class
	c	component
	d	directive
	e	enum
	g	guard
	i	interface
	m	module
	p	pipe
	s	service

Order (generally)
new
serve
generate
test
build

STEPS:
npm install -g @angular/cli
ng new app
cd app
npm install
ng serve

Copy and Add package.json and tslint.json and \src folder and then Restore

Controllers
[EnableCors(origins: "*", headers:"*", methods:"*")]

Angular
Observable, Subscriber
