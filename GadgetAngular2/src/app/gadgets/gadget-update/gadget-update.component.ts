import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Igadget } from '../gadget';
import { gadgetService } from '../gadget.service';

@Component({
  templateUrl: 'gadget-update.component.html',
  styleUrls: ['gadget-update.component.css']
})
export class gadgetUpdateComponent implements OnInit {
  pageTitle: string = 'gadget Update';
  errorMessage: string;
  gadget: Igadget;

  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _gadgetService: gadgetService) {
  }

  ngOnInit() {
    const param = this._route.snapshot.paramMap.get('id');
    if (param) {
      const id = +param;
      this.getgadget(id);
    }
  }

  getgadget(id: number) {
    this._gadgetService.getgadget(id).subscribe(
      gadget => this.gadget = gadget,
      error => this.errorMessage = <any>error);
  }

  Update(): void {
    this._gadgetService.makePutRequest().
    subscribe(
      error => this.errorMessage = <any>error);
      this._router.navigate(['/gadgets']);
  }

  onBack(): void {
    this._router.navigate(['/gadgets']);
  }

}
