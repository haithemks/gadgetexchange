import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { gadgetUpdateComponent } from './gadget-update.component';

describe('gadgetUpdateComponent', () => {
  let component: gadgetUpdateComponent;
  let fixture: ComponentFixture<gadgetUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ gadgetUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(gadgetUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
