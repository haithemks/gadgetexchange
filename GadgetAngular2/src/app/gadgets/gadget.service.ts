import { Injectable } from '@angular/core';
import { HttpClientModule , HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { Igadget } from './gadget';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
// tslint:disable-next-line:class-name
export class gadgetService {
 // private _gadgetUrl = './api/gadgets/gadgets.json';

  private _GadgetUrl = 'http://www.haithem-araissia.com/gadgetexchange/api/gadget';

    constructor(private _http: HttpClient) { }

    getgadgets(): Observable<Igadget[]> {
      return this._http.get<Igadget[]>(this._GadgetUrl)
        .do(data => console.log('All: ' + JSON.stringify(data)))
        .do(data => data.forEach((item, index) => {
          item.ImagePath = 'http://www.haithem-araissia.com/gadgetexchange/' + item.ImagePath;
        }))
            .catch(this.handleError);
    }

    getgadget(id: number): Observable<Igadget> {
        return this.getgadgets()
          .map((gadgets: Igadget[]) => {
            const gadget = gadgets.find(p => p.Id === id);
            return gadget;
          });
    }

 Updategadget(id: number, gadget: Igadget): Observable<Igadget> {
    return this.getgadgets()
      .map((gadgets: Igadget[]) => {
        // tslint:disable-next-line:no-shadowed-variable
        const gadget = gadgets.find(p => p.Id === id);
        return gadget;
      });
  }

  makePutRequest(): Observable<any[]> {
  {
    const bodyString = JSON.stringify('{2}'); // Stringify payload
    const headers  = new HttpHeaders({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    //  const options   = new RequestOptions({ headers: headers }); // Create a request option
    return this._http.put('https://localhost:44382/api/Gadget/2', bodyString  ) // ...using post request
                     //.map((res: Response) => res.json()) // ...and calling .json() on the response to return data
                     .catch((error: any) =>
                     Observable.throw(error.message || 'Server error')); //...errors if any
  }
}

  private extractData(res: Response) {
    const body = res.json();
    return body;
  }

  Deletegadget(id: number): Observable<Igadget> {
    return this.getgadgets()
      .map((gadgets: Igadget[]) => {
        const gadget = gadgets.find(p => p.Id === id);
        return gadget;
      });
  }
    private handleError(err: HttpErrorResponse) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        let errorMessage = '';
        if (err.error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage);
        return Observable.throw(errorMessage);
    }
}
