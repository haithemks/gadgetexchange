import { NgModule } from '@angular/core';
import { gadgetListComponent } from './gadget-list/gadget-list.component';
import { gadgetDetailComponent } from './gadget-detail/gadget-detail.component';
import { gadgetUpdateComponent } from './gadget-update/gadget-update.component';
import { gadgetDeleteComponent } from './gadget-delete/gadget-delete.component';
import { RouterModule } from '@angular/router';
import { gadgetGuardService } from './gadget-guard.service';
import { gadgetService } from './gadget.service';
import { SharedModule } from './../shared/shared.module';

@NgModule({
  imports: [
    RouterModule.forChild([
        { path: 'gadgets', component: gadgetListComponent },
        { path: 'gadgets/:id',
          canActivate: [ gadgetGuardService ],
          component: gadgetDetailComponent
        },
      {
        path: 'gadgetUpdate/:id',
        canActivate: [gadgetGuardService],
        component: gadgetUpdateComponent
        },
      {
        path: 'gadgetDelete/:id',
        canActivate: [gadgetGuardService],
        component: gadgetDeleteComponent
      }
    ]),
    SharedModule
  ],
  declarations: [
    gadgetListComponent,
    gadgetDetailComponent,
    gadgetUpdateComponent,
    gadgetDeleteComponent
  ],
  providers: [
    gadgetService,
    gadgetGuardService
  ]
})
export class GadgetModule { }
