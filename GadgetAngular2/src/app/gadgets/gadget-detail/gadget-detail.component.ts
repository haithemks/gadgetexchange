import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Igadget } from '../gadget';
import { gadgetService } from '../gadget.service';

@Component({
  templateUrl: 'gadget-detail.component.html',
  styleUrls: ['gadget-detail.component.css']
})

export class gadgetDetailComponent implements OnInit {
  pageTitle: string = 'gadget Detail';
  errorMessage: string;
  gadget: Igadget;

  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _gadgetService: gadgetService) {
  }

  ngOnInit() {
    const param = this._route.snapshot.paramMap.get('id');
    if (param) {
      const id = +param;
      this.getgadget(id);
    }
  }

  getgadget(id: number) {
    this._gadgetService.getgadget(id).subscribe(
      gadget => this.gadget = gadget,
      error => this.errorMessage = <any>error);
  }

  onBack(): void {
    this._router.navigate(['/gadgets']);
  }

}
