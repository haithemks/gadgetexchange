import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { gadgetDetailComponent } from 'gadget-detail.component';

describe('gadgetDetailComponent', () => {
  let component: gadgetDetailComponent;
  let fixture: ComponentFixture<gadgetDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ gadgetDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(gadgetDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
