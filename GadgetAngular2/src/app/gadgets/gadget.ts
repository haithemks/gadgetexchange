export interface Igadget {
  Id: number;
  Name: string;
  ImagePath: string;
  Description: string;
  ReviewsCount: number;
  StatusId: number;
  Cost: number;
}

