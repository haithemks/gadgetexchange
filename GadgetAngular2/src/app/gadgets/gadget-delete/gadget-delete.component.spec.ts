import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { gadgetDeleteComponent } from './gadget-delete.component';

describe('gadgetDeleteComponent', () => {
  let component: gadgetDeleteComponent;
  let fixture: ComponentFixture<gadgetDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ gadgetDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(gadgetDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
