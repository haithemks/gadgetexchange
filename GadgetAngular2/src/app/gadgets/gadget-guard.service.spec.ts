import { TestBed, inject } from '@angular/core/testing';

import { gadgetGuardService } from './gadget-guard.service';

describe('gadgetGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [gadgetGuardService]
    });
  });

  it('should be created', inject([gadgetGuardService], (service: gadgetGuardService) => {
    expect(service).toBeTruthy();
  }));
});
