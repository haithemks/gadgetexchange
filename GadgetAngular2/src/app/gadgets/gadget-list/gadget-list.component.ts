import { Component, OnInit } from '@angular/core';
import { Igadget } from '../gadget';
import { gadgetService } from '../gadget.service';

@Component({
    templateUrl: 'gadget-list.component.html',
    styleUrls: ['gadget-list.component.css']
})

export class gadgetListComponent implements OnInit {
    pageTitle: string = 'Gadgets';
    imageWidth: number = 50;
    imageMargin: number = 2;
    showImage: boolean = false;
    errorMessage: string;

    _listFilter: string;
    get listFilter(): string {
        return this._listFilter;
    }
    set listFilter(value: string) {
        this._listFilter = value;
        this.filteredgadgets = this.listFilter ? this.performFilter(this.listFilter) : this.gadgets;
    }

    filteredgadgets: Igadget[];
    gadgets: Igadget[] = [];

    constructor(private _gadgetService: gadgetService) {

    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'gadget List: ' + message;
    }

    performFilter(filterBy: string): Igadget[] {
        filterBy = filterBy.toLocaleLowerCase();
        return this.gadgets.filter((gadget: Igadget) =>
              gadget.Name.toLocaleLowerCase().indexOf(filterBy) !== -1);
    }

    toggleImage(): void {
        this.showImage = !this.showImage;
    }

    ngOnInit(): void {
        this._gadgetService.getgadgets()
                .subscribe(gadgets => {
                    this.gadgets = gadgets;
                    this.filteredgadgets = this.gadgets;
                },
                    error => this.errorMessage = <any>error);
    }
}
