import { Component } from '@angular/core';

@Component({
  selector: 'pm-root',
  template: `
<nav class="navbar navbar-default custom-header">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand navbar-link" href="#">Gadget<span>Exchange </span></a>
      <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
    </div>
    <div class="collapse navbar-collapse" id="navbar-collapse">
      <ul class="nav navbar-nav links">
        <li role="presentation"><a [routerLink]="['/welcome']">Home</a></li>
        <li role="presentation"><a [routerLink]="['/gadgets']">Gadgets</a></li>
        <li role="presentation"><a href="#" class="custom-navbar">Upgrades<span class="badge">new</span></a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#"><span class="caret"></span>
            <img src="assets/img/avatar.jpg" class="dropdown-image"></a>
          <ul class="dropdown-menu dropdown-menu-right" role="menu">
            <li role="presentation"><a href="#">Settings </a></li>
            <li role="presentation"><a href="#">Payments </a></li>
            <li role="presentation" class="active"><a href="#">Logout </a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="container gallery-container">
<h1>Mar<span style="color: black">Ket</span>
</h1>
</div>
<div class='container'>
     <router-outlet></router-outlet>
   </div>
    `
})

export class AppComponent {
  pageTitle: string = 'GadgetExchange Angular2';
}
